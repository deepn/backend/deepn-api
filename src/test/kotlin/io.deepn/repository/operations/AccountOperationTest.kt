package io.deepn.repository.operations

import io.deepn.entity.Account
import io.deepn.utils.toQuery
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.stereotype.Service

@Service
class AccountOperationTest(private val template: ReactiveMongoTemplate) {

    fun setConfirmed(id: ObjectId, value: Boolean) =
        template.updateFirst(
            (Account::id isEqualTo id).toQuery(),
            Update().set("confirmation.confirmed", value),
            Account::class.java
        )

}