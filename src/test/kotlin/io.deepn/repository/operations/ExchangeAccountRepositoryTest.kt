package io.deepn.repository.operations

import io.deepn.entity.ExchangeAccount
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
interface ExchangeAccountRepositoryTest: ReactiveMongoRepository<ExchangeAccount, String> {

    fun findByName(name: String): Mono<ExchangeAccount>

}