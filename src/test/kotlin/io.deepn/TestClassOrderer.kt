package io.deepn

import org.junit.jupiter.api.ClassOrderer
import org.junit.jupiter.api.ClassOrdererContext

class TestClassOrderer : ClassOrderer {

    override fun orderClasses(context: ClassOrdererContext) {
        context.classDescriptors.sortWith(
                Comparator.comparingInt { descriptor ->
                    return@comparingInt descriptor
                            .findAnnotation(ClassOrder::class.java)
                            .map { it.value }
                            .orElse(0)
                }
        )
    }

}