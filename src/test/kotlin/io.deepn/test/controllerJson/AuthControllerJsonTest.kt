package io.deepn.test.controllerJson

import io.deepn.ClassOrder
import io.deepn.CommonFunction.bodyInserter
import io.deepn.CommonFunction.copy
import io.deepn.CommonFunction.dataClassToMap
import io.deepn.CommonFunction.deleteAllAccount
import io.deepn.CommonFunction.isLessThanAMinute
import io.deepn.SharedMemory.STRING_26_CHAR
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.STRING_61_CHAR
import io.deepn.SharedMemory.STRING_7_CHAR
import io.deepn.SharedMemory.accountRequest
import io.deepn.SharedMemory.client
import io.deepn.SharedMemory.loginRequest
import io.deepn.entity.AccountRole
import io.deepn.entity.LoginResponseTest
import org.junit.jupiter.api.*
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

@ClassOrder(21)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class AuthControllerJsonTest {

    companion object {
        const val ENDPOINT = "auth"
        val jsonAccountRequest = dataClassToMap(accountRequest)
        val jsonLoginRequest = dataClassToMap(loginRequest)

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            deleteAllAccount()
        }

    }

    @Test
    @Order(1)
    fun `badRequest - createAccount - wrong parameter`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(mapOf("register" to "register")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(2)
    fun `badRequest - createAccount - email is number`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["email"] = "1234567" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(3)
    fun `badRequest - createAccount - email isn't an email`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["email"] = "bob@.com" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - createAccount - email has space`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["email"] = "bo bo @gmail" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - createAccount - email has multiple space`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["email"] = "ab     cd az    efg" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - createAccount - email isEmpty`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["email"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - createAccount - email null`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["email"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `badRequest - createAccount - doesn't have email`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it.remove("email") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(9)
    fun `badRequest - createAccount - username isEmpty`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(10)
    fun `badRequest - createAccount - username null`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(11)
    fun `badRequest - createAccount - doesn't have username`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it.remove("username") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(12)
    fun `badRequest - createAccount - username too short`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = STRING_3_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(13)
    fun `badRequest - createAccount - username too long`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = STRING_26_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(14)
    fun `badRequest - createAccount - username has special character`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = "abcd$" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(15)
    fun `badRequest - createAccount - username has space`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = "abcd azefg" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(16)
    fun `badRequest - createAccount - username has multiple space`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = "ab     cd az    efg" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(17)
    fun `badRequest - createAccount - password too short`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["password"] = STRING_7_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(18)
    fun `badRequest - createAccount - password too long`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["password"] = STRING_61_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(19)
    fun `badRequest - createAccount - password null`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["password"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(20)
    fun `badRequest - createAccount - doesn't have password`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it.remove("password") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(21)
    fun `isCreated - createAccount`() {
        val requestBody = client.post().uri("$ENDPOINT/register").contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java)
            .returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertEquals(jsonAccountRequest["username"], responseBody?.account?.username)
        Assertions.assertEquals(jsonAccountRequest["email"], responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
    }

    @Test
    @Order(22)
    fun `conflict - createAccount - already exists`() {
        client.post().uri("$ENDPOINT/register").contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(23)
    fun `conflict - createAccount - email used but available username`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["username"] = "B0b0tan" }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(24)
    fun `conflict - createAccount - username used but available email`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAccountRequest.copy { it["email"] = "B0b0t@dc.com" }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(25)
    fun `badRequest loginAccount - email wrongly formatted`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["login"] = "bobo" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(26)
    fun `badRequest loginAccount - email null`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["login"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(27)
    fun `badRequest loginAccount - doesn't have email`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it.remove("login") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(28)
    fun `unauthorized - loginAccount - email doesn't exist`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["login"] = "te@test.fr" }))
            .exchange().expectStatus().isUnauthorized
    }

    @Test
    @Order(29)
    fun `badRequest - loginAccount - email empty`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["login"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(30)
    fun `unauthorized - loginAccount - wrong password`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["password"] = "azerty" }))
            .exchange().expectStatus().isUnauthorized
    }

    @Test
    @Order(31)
    fun `badRequest - loginAccount - password empty`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["password"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(32)
    fun `badRequest loginAccount - password null`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["password"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(33)
    fun `badRequest loginAccount - doesn't have password`() {
        client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it.remove("password") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(34)
    fun `isOk - loginAccount`() {
        val requestBody = client.post().uri("$ENDPOINT/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(jsonAccountRequest["username"], responseBody?.account?.username)
        Assertions.assertEquals(jsonAccountRequest["email"], responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
    }

}
