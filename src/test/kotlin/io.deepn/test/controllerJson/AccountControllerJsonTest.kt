package io.deepn.test.controllerJson

import io.deepn.ClassOrder
import io.deepn.CommonFunction
import io.deepn.CommonFunction.blockingGet
import io.deepn.CommonFunction.bodyInserter
import io.deepn.CommonFunction.copy
import io.deepn.CommonFunction.dataClassToMap
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.STRING_61_CHAR
import io.deepn.SharedMemory.STRING_7_CHAR
import io.deepn.SharedMemory.accountRepository
import io.deepn.SharedMemory.accountRequest
import io.deepn.SharedMemory.client
import io.deepn.SharedMemory.loginRequest
import io.deepn.SharedMemory.token
import io.deepn.entity.AccountRole
import io.deepn.entity.AccountTest
import io.deepn.entity.LoginResponseTest
import io.deepn.entity.request.ConfirmationCode
import io.deepn.entity.response.Ok
import org.bson.types.ObjectId
import org.junit.jupiter.api.*
import org.springframework.http.MediaType

@ClassOrder(22)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class AccountControllerJsonTest {

    companion object {
        var accountId: ObjectId? = null
        var confirmationCode: String? = null
        const val ENDPOINT = "account"
        val jsonPasswordUpdateRequest = mapOf(
            "currentPassword" to loginRequest.password,
            "newPassword" to "newPassword"
        )
        val jsonLoginRequest = dataClassToMap(loginRequest)
        val jsonConfirmationCode by lazy {
            dataClassToMap(ConfirmationCode(confirmationCode!!))
        }

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            CommonFunction.createAccountAndTest()
        }

    }

    @Test
    @Order(1)
    fun getAccount() {
        val requestBody = client.get().uri("$ENDPOINT/")
            .accept(MediaType.APPLICATION_JSON)
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AccountTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(accountRequest.username, responseBody?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.email)
        Assertions.assertEquals(false, responseBody?.confirmed)
        Assertions.assertNull(responseBody?.confirmationCode)
        Assertions.assertNull(responseBody?.confirmationSendTime)
        Assertions.assertNull(responseBody?.resetToken)
        Assertions.assertNull(responseBody?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.role)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        accountId = responseBody!!.id
    }

    @Test
    @Order(2)
    fun `badRequest - updatePassword - missing currentPassword`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(mapOf("newPassword" to "newPassword")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(3)
    fun `badRequest - updatePassword - missing newPassword`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(mapOf("currentPassword" to "currentPassword")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - updatePassword - missing parameter`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - updatePassword - wrong parameter`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(mapOf("parameter" to "currentPassword")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - updatePassword - current password too short`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["currentPassword"] = STRING_3_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - updatePassword - current password too long`() {
        client.put().uri("$ENDPOINT/update-password")
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["currentPassword"] = STRING_61_CHAR }))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `badRequest - updatePassword - current password is empty`() {
        client.put().uri("$ENDPOINT/update-password")
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["currentPassword"] = "" }))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(9)
    fun `badRequest - updatePassword - current password null`() {
        client.put().uri("$ENDPOINT/update-password")
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["currentPassword"] = null }))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }


    @Test
    @Order(10)
    fun `badRequest - updatePassword - new password too short`() {
        client.put().uri("$ENDPOINT/update-password")
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["newPassword"] = STRING_3_CHAR }))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(11)
    fun `badRequest - updatePassword - new password too long`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["newPassword"] = STRING_61_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(12)
    fun `badRequest - updatePassword - new password is empty`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["newPassword"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(13)
    fun `badRequest - updatePassword - new password null`() {
        client.put().uri("$ENDPOINT/update-password")
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["newPassword"] = null }))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(14)
    fun `unauthorized - updatePassword - current password doesn't match`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(jsonPasswordUpdateRequest.copy { it["currentPassword"] = "azertyui" }))
            .exchange().expectStatus().isUnauthorized
    }

    @Test
    @Order(15)
    fun `isOk - login - before changing password`() {
        val requestBody = client.post().uri("auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java)
            .returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(accountRequest.username, responseBody?.account?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
    }

    @Test
    @Order(16)
    fun `noContent - updatePassword - success - true`() {
        client.put().uri("$ENDPOINT/update-password")
            .headers { it.setBearerAuth(token) }
            .body(bodyInserter(jsonPasswordUpdateRequest))
            .exchange().expectStatus().isNoContent
            .expectBody().isEmpty
    }

    @Test
    @Order(17)
    fun `isOk - login - after changing password`() {
        val requestBody = client.post().uri("auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest.copy { it["password"] = jsonPasswordUpdateRequest["newPassword"] }))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java)
            .returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(accountRequest.username, responseBody?.account?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
    }

    @Test
    @Order(18)
    fun `unauthorized - login - wrong password`() {
        client.post().uri("auth/login")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonLoginRequest))
            .exchange().expectStatus().isUnauthorized
            .expectBody().isEmpty
    }

    @Test
    @Order(19)
    fun `generate confirm code and check update`() {
        Assertions.assertNotNull(accountId)
        accountRepository.findById(accountId.toString()).blockingGet().apply {
            Assertions.assertNull(this?.confirmationCode)
            Assertions.assertNull(this?.confirmationSendTime)
            Assertions.assertEquals(false, this?.confirmed)
            println("Done findById")
        }

        val requestBody = client.post().uri("${ENDPOINT}/confirm")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()

        response.expectStatus().isOk
        Assertions.assertTrue(response.expectBody(Ok::class.java).returnResult().responseBody!!.success)

        accountRepository.findById(accountId.toString()).blockingGet().apply {
            Assertions.assertNotNull(this?.confirmationCode)
            Assertions.assertNotNull(this?.confirmationSendTime)
            Assertions.assertEquals(false, this?.confirmed)
            confirmationCode = this?.confirmationCode
            println("Done findById")
        }
    }

    @Test
    @Order(20)
    fun `isOk - confirm code - wrong confirmationCode success false`() {
        val requestBody = client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(
                jsonConfirmationCode.copy { it["code"] = if(confirmationCode == "9999") "9998" else "9999" }
            ))

        val response = requestBody.exchange()
        response.expectStatus().isOk
        Assertions.assertFalse(response.expectBody(Ok::class.java).returnResult().responseBody!!.success)
    }

    @Test
    @Order(21)
    fun `badRequest - confirm code - alphabet code`() {
        client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonConfirmationCode.copy { it["code"] = "abcd"}))
            .exchange().expectStatus().isOk
            .apply {
                Assertions.assertEquals(false, expectBody(Ok::class.java).returnResult().responseBody?.success)
            }
    }

    @Test
    @Order(22)
    fun `badRequest - confirm code - alphanumeric confirmationCode`() {
        client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonConfirmationCode.copy { it["code"] = "ab23"}))
            .exchange().expectStatus().isOk
            .apply {
                Assertions.assertEquals(false, expectBody(Ok::class.java).returnResult().responseBody?.success)
            }
    }

    @Test
    @Order(23)
    fun `badRequest - confirm code - too short`() {
        client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonConfirmationCode.copy { it["code"] = STRING_3_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(24)
    fun `badRequest - confirm code - too long`() {
        client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonConfirmationCode.copy { it["code"] = STRING_7_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(25)
    fun `badRequest - confirm code - null`() {
        client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonConfirmationCode.copy { it["code"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(26)
    fun `badRequest - confirm code - no parameter`() {
        client.patch().uri("$ENDPOINT/confirm")
            .contentType(MediaType.APPLICATION_JSON)
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(27)
    fun `badRequest - confirm code - wrong parameter name`() {
        client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(mapOf("coda" to "1234")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(28)
    fun `badRequest - confirm code - isEmpty`() {
        client.patch().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonConfirmationCode.copy { it["code"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(29)
    fun `isOk confirm code success true`() {
        val requestBody = client.patch().uri("$ENDPOINT/confirm")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonConfirmationCode))
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        response.expectStatus().isOk
        Assertions.assertTrue(response.expectBody(Ok::class.java).returnResult().responseBody!!.success)
    }

}