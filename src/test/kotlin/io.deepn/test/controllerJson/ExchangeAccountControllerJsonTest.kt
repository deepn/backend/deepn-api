package io.deepn.test.controllerJson

import io.deepn.ClassOrder
import io.deepn.CommonFunction
import io.deepn.CommonFunction.bodyInserter
import io.deepn.CommonFunction.confirmCurrentAccount
import io.deepn.CommonFunction.copy
import io.deepn.CommonFunction.createAccountAndTest
import io.deepn.CommonFunction.dataClassToMap
import io.deepn.CommonFunction.deleteAllExchangeAccount
import io.deepn.SharedMemory.STRING_26_CHAR
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.accountResponse
import io.deepn.SharedMemory.client
import io.deepn.SharedMemory.exchangeAccount
import io.deepn.SharedMemory.exchangeRequest
import io.deepn.SharedMemory.token
import io.deepn.common.model.Exchange
import io.deepn.entity.ExchangeAccount
import org.bson.types.ObjectId
import org.junit.jupiter.api.*
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

@ClassOrder(23)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class ExchangeAccountControllerJsonTest {

    companion object {
        private const val ENDPOINT = "/exchange-account"
        var exchangeAccount2: ExchangeAccount? = null
        var exchangeAccount3: ExchangeAccount? = null
        val jsonExchangeRequest = mapOf(
            "name" to "say my name say my name",
            "exchange" to Exchange.BINANCE,
            "credentials" to mapOf(
                "key" to "key",
                "secret" to "secret",
            )
        )
        val exchangeRequest2 = mapOf(
            "name" to "Exchange2",
            "exchange" to Exchange.FTX,
            "credentials" to mapOf(
                "key" to "key2",
                "secret" to "secret2",
                "passphrase" to "passphrase2"
            )
        )
        val exchangeRequest3 = mapOf(
            "name" to "Exchange3",
            "exchange" to Exchange.BITFINEX,
            "credentials" to mapOf(
                "key" to "key3",
                "secret" to "secret3",
                "passphrase" to "passphrase3"
            ),
            "extra" to "extraParameter"
        )

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            createAccountAndTest()
            confirmCurrentAccount()
        }

        @AfterAll
        @JvmStatic
        fun deleteAccount() {
            deleteAllExchangeAccount()
        }
    }


    @Test
    @Order(1)
    fun `badRequest - create Exchange - name too short`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = STRING_3_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(2)
    fun `badRequest - create Exchange - name too long`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = STRING_26_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(3)
    fun `badRequest - create Exchange - name null`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - create Exchange - name empty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - create Exchange - name removed`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it.remove("name") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - create Exchange - exchange isn't in enum`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = "DEEPN" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - create Exchange - exchange empty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `badRequest - create Exchange - exchange null`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(9)
    fun `badRequest - create Exchange - exchange removed`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it.remove("exchange") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(10)
    fun `badRequest - create Exchange - credentials removed`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it.remove("credentials") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(11)
    fun `badRequest - create Exchange - credentialsKey removed`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it.remove("key") }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(12)
    fun `badRequest - create Exchange - credentialsKey null`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["key"] = null }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(13)
    fun `badRequest - create Exchange - credentialsKey empty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["key"] = "" }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(14)
    fun `badRequest - create Exchange - credentialsSecret removed`() {

        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it.remove("secret") }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(15)
    fun `badRequest - create Exchange - credentialsSecret null`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["secret"] = null }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(16)
    fun `badRequest - create Exchange - credentialsSecret empty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["secret"] = "" }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(17)
    fun `badRequest - create Exchange - credentialsPassphrase empty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["passphrase"] = "" }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(18)
    fun `badRequest - create Exchange - exchange and name`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["exchange"] = exchangeRequest.name
                it["name"] = exchangeRequest.exchange
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(19)
    fun `isCreated - create Exchange - all good parameters`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(jsonExchangeRequest["name"], responseBody?.name)
        Assertions.assertEquals(jsonExchangeRequest["exchange"], responseBody?.exchange)
        Assertions.assertEquals(jsonExchangeRequest["credentials"], dataClassToMap(responseBody?.credentials))
        exchangeAccount = responseBody
    }

    @Test
    @Order(20)
    fun `isCreated - create Exchange - all good parameters + extra parameter ignored`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(exchangeRequest3))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(exchangeRequest3["name"], responseBody?.name)
        Assertions.assertEquals(exchangeRequest3["exchange"], responseBody?.exchange)
        Assertions.assertEquals(exchangeRequest3["credentials"], dataClassToMap(responseBody?.credentials))
        exchangeAccount3 = responseBody
    }

    @Order(21)
    @Test
    fun `conflict - create Exchange - already exists`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))

        requestBody.exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(22)
    fun `conflict - create Exchange - name in lower case already exists`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = exchangeRequest.name.toLowerCase() }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(23)
    fun `conflict - create Exchange - name in upper case already exists`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["body"]= exchangeRequest.name.toUpperCase() }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(24)
    fun `conflict - create Exchange - name already exists different exchange`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = Exchange.COINBASE }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(25)
    fun `isOk - getAllExchange - isNotEmpty`() {
        val requestBody = client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
        val exchange = responseBody?.first()
        Assertions.assertEquals(exchangeAccount, exchange)
    }

    @Test
    @Order(26)
    fun `isOk - get Exchange BINANCE`() {
        val requestBody = client.get().uri("$ENDPOINT/${Exchange.BINANCE}")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        val exchange = responseBody?.first()
        Assertions.assertEquals(exchangeAccount, exchange)
    }

    @Test
    @Order(27)
    fun `isOk - get Exchange BITFINEX`() {
        val requestBody = client.get().uri("$ENDPOINT/${Exchange.BITFINEX}")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        val exchange = responseBody?.first()
        Assertions.assertEquals(exchangeAccount3, exchange)
    }

    @Test
    @Order(28)
    fun `isCreated - create Exchange2 - all good parameters`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(exchangeRequest2))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(exchangeRequest2["name"], responseBody?.name)
        Assertions.assertEquals(exchangeRequest2["exchange"], responseBody?.exchange)
        Assertions.assertEquals(exchangeRequest2["credentials"], dataClassToMap(responseBody?.credentials))

        exchangeAccount2 = responseBody
    }

    @Test
    @Order(29)
    fun `isOk - updateExchange - different name`() {
        val requestBody = client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = "MyName" }))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals("MyName", responseBody?.name)
        Assertions.assertEquals(Exchange.BINANCE, responseBody?.exchange)
        Assertions.assertEquals(jsonExchangeRequest["credentials"], dataClassToMap(responseBody?.credentials))
        exchangeAccount = responseBody
    }

    @Test
    @Order(30)
    fun `isOk - updateExchange - different exchange`() {
        val requestBody = client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = Exchange.FTX }))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(jsonExchangeRequest["name"], responseBody?.name)
        Assertions.assertEquals(Exchange.FTX, responseBody?.exchange)
        Assertions.assertEquals(jsonExchangeRequest["credentials"], dataClassToMap(responseBody?.credentials))
        exchangeAccount = responseBody
    }

    @Test
    @Order(31)
    fun `isOk - getAllExchange - 3 elements`() {
        val requestBody = client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
        Assertions.assertEquals(3, responseBody?.size)
        println(responseBody)
        val exchange1 = responseBody?.first()
        val exchange2 = responseBody?.get(2)
        val exchange3 = responseBody?.get(1)
        Assertions.assertEquals(exchangeAccount, exchange1)
        Assertions.assertEquals(exchangeAccount2, exchange2)
        Assertions.assertEquals(exchangeAccount3, exchange3)
    }

    @Test
    @Order(32)
    fun `isOk - get Exchange FTX - 2 elements`() {
        val requestBody = client.get().uri("$ENDPOINT/${Exchange.FTX}")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
        Assertions.assertEquals(2, responseBody?.size)
        println(responseBody)
        val exchange1 = responseBody?.first()
        val exchange2 = responseBody?.get(1)
        Assertions.assertEquals(exchangeAccount, exchange1)
        Assertions.assertEquals(exchangeAccount2, exchange2)
    }

    @Test
    @Order(33)
    fun `badRequest - update Exchange - name too short`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = STRING_3_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(34)
    fun `badRequest - update Exchange - name too long`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = STRING_26_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(35)
    fun `badRequest - update Exchange - name null`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(36)
    fun `badRequest - update Exchange - name empty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["name"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(37)
    fun `badRequest - update Exchange - name removed`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it.remove("name") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(38)
    fun `badRequest - update Exchange - exchange isn't in enum`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = "DEEPN" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(39)
    fun `badRequest - update Exchange - exchange empty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(40)
    fun `badRequest - update Exchange - exchange null`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it["exchange"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(41)
    fun `badRequest - update Exchange - exchange removed`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it.remove("exchange") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(42)
    fun `badRequest - update Exchange - credentials removed`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy { it.remove("credentials") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(43)
    fun `badRequest - update Exchange - credentialsKey removed`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it.remove("key") }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(44)
    fun `badRequest - update Exchange - credentialsKey null`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["key"] = null }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(45)
    fun `badRequest - update Exchange - credentialsKey empty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["key"] = "" }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(46)
    fun `badRequest - update Exchange - credentialsSecret removed`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it.remove("secret") }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(47)
    fun `badRequest - update Exchange - credentialsSecret null`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["secret"] = null }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(48)
    fun `badRequest - update Exchange - credentialsSecret empty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["secret"] = "" }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(49)
    fun `badRequest - update Exchange - credentialsPassphrase empty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["credentials"] = (it["credentials"] as MutableMap<String, Any?>).copy { it["passphrase"] = "" }
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(50)
    fun `badRequest - update Exchange - exchange and name`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest.copy {
                it["exchange"] = exchangeRequest.name
                it["name"] = exchangeRequest.exchange
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(51)
    fun `badRequest - update Exchange - wrong ID`() {
        client.put().uri("$ENDPOINT/${ObjectId()}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(52)
    fun `badRequest - update Exchange - ID isn't ObjectId`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}12")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(53)
    fun `badRequest - delete Exchange - wrong ID`() {
        client.delete().uri("$ENDPOINT/${ObjectId()}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(54)
    fun `badRequest - delete Exchange - ID isn't ObjectId`() {
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}12")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(55)
    fun `isOk - delete Exchange - good ID`() {
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isOk
    }

    @Test
    @Order(56)
    fun `notFound - delete Exchange - already Deleted`() {
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isNotFound
    }

    /**
     * NO BEARER REQUEST PART
     */

    @Test
    @Order(57)
    fun `isCreated - create Exchange - to make sure we don't have access`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(jsonExchangeRequest["name"], responseBody?.name)
        Assertions.assertEquals(jsonExchangeRequest["exchange"], responseBody?.exchange)
        Assertions.assertEquals(jsonExchangeRequest["credentials"], dataClassToMap(responseBody?.credentials))

        exchangeAccount = responseBody
    }

    @Test
    @Order(58)
    fun `unauthorized - account no bearer`() {
        println("----\nGetAll\n----")
        client.get().uri("$ENDPOINT/")
            .exchange().expectStatus().isUnauthorized

        println("----\nGet By Exchange\n----")
        client.get().uri("$ENDPOINT/${Exchange.BINANCE}")
            .exchange().expectStatus().isUnauthorized

        println("----\nCreate Exchange\n----")
        client.post().uri("$ENDPOINT/")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))
            .exchange().expectStatus().isUnauthorized

        println("----\nUpdate Exchange\n----")
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))
            .exchange().expectStatus().isUnauthorized

        println("----\nDelete Exchange\n----")
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .exchange().expectStatus().isUnauthorized
    }


    /**
     * UNCONFIRMED ACCOUNT PART
     */

    @Test
    @Order(59)
    fun unconfirmCurrentAccount() = confirmCurrentAccount(false)

    @Test
    @Order(60)
    fun `unauthorized - account not confirmed`() {
        println("----\nGetAll\n----")
        client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isUnauthorized

        println("----\nGet By Exchange\n----")
        client.get().uri("$ENDPOINT/${Exchange.BINANCE}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isUnauthorized

        println("----\nCreate Exchange\n----")
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))
            .exchange().expectStatus().isUnauthorized

        println("----\nUpdate Exchange\n----")
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonExchangeRequest))
            .exchange().expectStatus().isUnauthorized

        println("----\nDelete Exchange\n----")
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isUnauthorized
    }
}