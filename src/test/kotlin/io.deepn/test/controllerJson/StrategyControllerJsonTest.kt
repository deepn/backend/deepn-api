package io.deepn.test.controllerJson

import io.deepn.ClassOrder
import io.deepn.CommonFunction
import io.deepn.CommonFunction.bodyInserter
import io.deepn.CommonFunction.confirmCurrentAccount
import io.deepn.CommonFunction.copy
import io.deepn.CommonFunction.createAccountAndTest
import io.deepn.CommonFunction.dataClassToMap
import io.deepn.CommonFunction.deleteAllExchangeAccount
import io.deepn.CommonFunction.deleteAllStrategy
import io.deepn.CommonFunction.getOrCreateExchangeAccount
import io.deepn.SharedMemory
import io.deepn.SharedMemory.STRING_200_PLUS_CHAR
import io.deepn.SharedMemory.STRING_26_CHAR
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.STRING_4000_PLUS_CHAR
import io.deepn.SharedMemory.algorithmicStrategyResponse
import io.deepn.SharedMemory.exchangeAccount
import io.deepn.SharedMemory.tradingConfiguration
import io.deepn.common.model.BotStatus
import io.deepn.common.model.StrategyType
import io.deepn.entity.AlgorithmicStrategyRequestTest
import io.deepn.entity.AlgorithmicStrategyTest
import io.deepn.entity.StrategyTest
import io.deepn.entity.response.Ok
import io.deepn.entity.strategy.impl.AlgorithmicStrategy
import org.bson.types.ObjectId
import org.junit.jupiter.api.*
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

@ClassOrder(24)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class StrategyControllerJsonTest {

    companion object {
        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            createAccountAndTest()
            deleteAllExchangeAccount()
            deleteAllStrategy()
            exchangeAccount = getOrCreateExchangeAccount()
            confirmCurrentAccount()
        }

        @AfterAll
        @JvmStatic
        internal fun afterAll() {
            deleteAllExchangeAccount()
            deleteAllStrategy()
        }

        const val ENDPOINT = "/strategy"
        const val ALGO_ENDPOINT = "$ENDPOINT/algorithmic"
        val jsonAlgorithmicStrategyRequest by lazy {
            dataClassToMap(AlgorithmicStrategyRequestTest(
                "My Script",
                "Description of my script",
                "a = 'ok'",
                exchangeAccount!!.id,
                tradingConfiguration
            ))
        }
        val jsonTradingConfiguration by lazy {
            jsonAlgorithmicStrategyRequest["configuration"] as MutableMap<String, Any?>
        }
        val jsonLongTradingConfiguration by lazy {
            jsonTradingConfiguration["longConfiguration"] as MutableMap<String, Any?>
        }
        val jsonShortTradingConfiguration by lazy {
            jsonTradingConfiguration["shortConfiguration"] as MutableMap<String, Any?>
        }
        val jsonTradeConfiguration by lazy {
            jsonTradingConfiguration["tradeConfiguration"] as MutableMap<String, Any?>
        }

        fun jsonTradingConfiguration(key: String, value: Any?): MutableMap<String, Any?> =
             jsonAlgorithmicStrategyRequest.copy {
                 it["configuration"] = (it["configuration"] as Map<String, Any?>).copy {
                     it[key] = value
                 }
             }.also { println(it) }

        fun jsonTradingConfigurationRemoveValue(value: String): MutableMap<String, Any?> =
            jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy {
                    it.remove(value)
                }
            }.also { println(it) }

    }

    /**
     * CONFIRMED ACCOUNT PART
     */

    /**
     * ALGORITHMIC
     */

    @Test
    @Order(1)
    fun `badRequest - create AlgorithmicStrategy - name is null`() {
        println(jsonTradingConfiguration)
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(2)
    fun `badRequest - create AlgorithmicStrategy - name is isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(3)
    fun `badRequest - create AlgorithmicStrategy - name is isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - create AlgorithmicStrategy - name is removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("name") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - create AlgorithmicStrategy - name too short`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["exchange"] = exchangeAccount!!.id
                it["name"] = STRING_3_CHAR
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - create AlgorithmicStrategy - name too long`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["exchange"] = exchangeAccount!!.id
                it["name"] = STRING_26_CHAR
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - create AlgorithmicStrategy - description too long`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["exchange"] = exchangeAccount!!.id
                it["description"] = STRING_200_PLUS_CHAR
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `badRequest - create AlgorithmicStrategy - description is null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["description"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(9)
    fun `badRequest - create AlgorithmicStrategy - description is removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("description") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(10)
    fun `badRequest - create AlgorithmicStrategy - source too long`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = STRING_4000_PLUS_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(11)
    fun `badRequest - create AlgorithmicStrategy - source is null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(12)
    fun `badRequest - create AlgorithmicStrategy - source is isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(13)
    fun `badRequest - create AlgorithmicStrategy - source is isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(14)
    fun `badRequest - create AlgorithmicStrategy - source is removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("source") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(15)
    fun `notFound - create AlgorithmicStrategy - exchange notFound`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["name"] = "notFound exchange"
                it["exchange"] = ObjectId().toString()
            }))
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(16)
    fun `badRequest - create AlgorithmicStrategy - exchange is not ObjectId`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = "123456" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(17)
    fun `badRequest - create AlgorithmicStrategy - exchange is null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(18)
    fun `badRequest - create AlgorithmicStrategy - exchange is isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(19)
    fun `badRequest - create AlgorithmicStrategy - exchange is isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(20)
    fun `badRequest - create AlgorithmicStrategy - exchange is removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("exchange") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(21)
    fun `badRequest - create AlgorithmicStrategy - configuration is null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(22)
    fun `badRequest - create AlgorithmicStrategy - configuration is isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(23)
    fun `badRequest - create AlgorithmicStrategy - configuration wrong object`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = "configuration" }))
            .exchange().expectStatus().isBadRequest
    }


    @Test
    @Order(24)
    fun `badRequest - create AlgorithmicStrategy - configuration is isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(25)
    fun `badRequest - create AlgorithmicStrategy - configuration is removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("configuration") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(26)
    fun `badRequest - create AlgorithmicStrategy - quoteCurrency is null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["quoteCurrency"] = null }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(27)
    fun `badRequest - create AlgorithmicStrategy - quoteCurrency is isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["quoteCurrency"] = "" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(28)
    fun `badRequest - create AlgorithmicStrategy - quoteCurrency is isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["quoteCurrency"] = " " }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(29)
    fun `badRequest - create AlgorithmicStrategy - quoteCurrency is removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it.remove("quoteCurrency") }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(30)
    fun `badRequest - create AlgorithmicStrategy - currencyList list is empty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = listOf<String>()}}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(31)
    fun `badRequest - create AlgorithmicStrategy - currencyList list of null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = listOf(null)}}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(32)
    fun `badRequest - create AlgorithmicStrategy - currencyList is null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = null }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(33)
    fun `badRequest - create AlgorithmicStrategy - currencyList isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = "" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(34)
    fun `badRequest - create AlgorithmicStrategy - currencyList isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = " " }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(35)
    fun `badRequest - create AlgorithmicStrategy - currencyList removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it.remove("currencies") }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(36)
    fun `badRequest - create AlgorithmicStrategy - currencyList wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = "abcde" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(37)
    fun `badRequest - create AlgorithmicStrategy - timeFrame is null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = null }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(38)
    fun `badRequest - create AlgorithmicStrategy - timeFrame is isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = "" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(39)
    fun `badRequest - create AlgorithmicStrategy - timeFrame is isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = " " }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(40)
    fun `badRequest - create AlgorithmicStrategy - timeFrame is wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = "abcde" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(41)
    fun `badRequest - create AlgorithmicStrategy - timeFrame is removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it.remove("timeFrame") }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(42)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", "MYTYPE")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(43)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", null)))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(44)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(45)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfigurationRemoveValue("longConfiguration")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(46)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_orderType wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["orderType"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(47)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_orderType isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["orderType"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(48)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_orderType isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["orderType"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(49)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_stopLossPercent wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["stopLossPercent"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(50)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_stopLossPercent isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["stopLossPercent"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(51)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_stopLossPercent isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["stopLossPercent"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(52)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_takeProfitPercent wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["takeProfitPercent"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(53)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_takeProfitPercent isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["takeProfitPercent"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(54)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_takeProfitPercent isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["takeProfitPercent"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(55)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_limitOffset wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["limitOffset"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(56)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_limitOffset isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["limitOffset"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(57)
    fun `badRequest - create AlgorithmicStrategy - longConfiguration_limitOffset isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["limitOffset"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(58)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", "MYTYPE")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(59)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", null)))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(60)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(61)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfigurationRemoveValue("shortConfiguration")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(62)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_orderType wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["orderType"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(63)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_orderType isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["orderType"] = "" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(64)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_orderType isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["orderType"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(65)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_stopLossPercent wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["stopLossPercent"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(66)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_stopLossPercent isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["stopLossPercent"] = "" })))
             .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(67)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_stopLossPercent isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["stopLossPercent"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(68)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_takeProfitPercent wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["takeProfitPercent"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(69)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_takeProfitPercent isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["takeProfitPercent"] = "" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(70)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_takeProfitPercent isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["takeProfitPercent"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(71)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_limitOffset wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["limitOffset"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(72)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_limitOffset isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["limitOffset"] = "" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(73)
    fun `badRequest - create AlgorithmicStrategy - shortConfiguration_limitOffset isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["limitOffset"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(74)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", "MYTYPE")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(75)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", null)))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(76)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(77)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration.copy { it.remove("tradeConfiguration")}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(78)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_minimumQuantity wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(79)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_minimumQuantity isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(80)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_minimumQuantity isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(81)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_minimumQuantity null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = null }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(82)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_minimumQuantity removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it.remove("minimumQuantity") }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(83)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumQuantity wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(84)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumQuantity isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(85)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumQuantity isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(86)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumQuantity null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = null }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(87)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumQuantity removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it.remove("maximumQuantity") }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(88)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_amendStakeAmount wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(89)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_amendStakeAmount isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(90)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_amendStakeAmount isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(91)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_amendStakeAmount null`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = null }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(92)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_amendStakeAmount removed`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it.remove("amendStakeAmount") }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(93)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumOrderPerCoin wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOrderPerCoin"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(94)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumOrderPerCoin isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOrderPerCoin"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(95)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumOrderPerCoin isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOrderPerCoin"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(96)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumOpenOrders wrong type`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOpenOrders"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(97)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumOpenOrders isEmpty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOpenOrders"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(98)
    fun `badRequest - create AlgorithmicStrategy - tradeConfiguration_maximumOpenOrders isBlank`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOpenOrders"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(99)
    fun `isCreated - create AlgorithmicStrategy - additionnal parameter`() {
        val map: Map<String, Any?> = mapOf(
            "name" to "Name",
            "description" to "Description",
            "source" to "Source",
            "exchange" to exchangeAccount!!.id.toString(),
            "configuration" to jsonTradingConfiguration
        )

        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(map.copy { it["configuration"] = jsonTradingConfiguration.copy { it["newParameter"] = 53 } }))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(map["name"], responseBody?.name)
        Assertions.assertEquals(map["configuration"], dataClassToMap(responseBody?.configuration))
        Assertions.assertEquals(map["exchange"], responseBody?.exchange?.toString())
        Assertions.assertEquals(map["description"], responseBody?.description)
        Assertions.assertEquals(map["source"], responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
    }

    @Test
    @Order(100)
    fun `isCreated - create AlgorithmicStrategy - all good params`() {
        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["name"], responseBody?.name)
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["configuration"], dataClassToMap(responseBody?.configuration))
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["exchange"], responseBody?.exchange?.toString())
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["description"], responseBody?.description)
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["source"], responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
        algorithmicStrategyResponse = responseBody
    }

    @Test
    @Order(102)
    fun `isCreated - create AlgorithmicStrategy - all good params name in hebrew`() {
        val request = jsonAlgorithmicStrategyRequest.copy { it["name"] = "האלפבית העברי" }
        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(request))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request["name"], responseBody?.name)
        Assertions.assertEquals(request["configuration"], dataClassToMap(responseBody?.configuration))
        Assertions.assertEquals(request["exchange"], responseBody?.exchange?.toString())
        Assertions.assertEquals(request["description"], responseBody?.description)
        Assertions.assertEquals(request["source"], responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
    }

    @Test
    @Order(103)
    fun `isCreated - create AlgorithmicStrategy - all good params name with special character1`() {
        val request = jsonAlgorithmicStrategyRequest.copy { it["name"] = "=+-&,;:`£ù%^*€\$\"'(§è!çà)" }
        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(request))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request["name"], responseBody?.name)
        Assertions.assertEquals(request["configuration"], dataClassToMap(responseBody?.configuration))
        Assertions.assertEquals(request["exchange"], responseBody?.exchange?.toString())
        Assertions.assertEquals(request["description"], responseBody?.description)
        Assertions.assertEquals(request["source"], responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
    }

    @Test
    @Order(104)
    fun `isCreated - create AlgorithmicStrategy - all good params name with special character2`() {
        val request = jsonAlgorithmicStrategyRequest.copy { it["name"] = "\uF8FFë“‘{¶«¡Çø}—••😀" }
        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(request))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request["name"], responseBody?.name)
        Assertions.assertEquals(request["configuration"], dataClassToMap(responseBody?.configuration))
        Assertions.assertEquals(request["exchange"], responseBody?.exchange?.toString())
        Assertions.assertEquals(request["description"], responseBody?.description)
        Assertions.assertEquals(request["source"], responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
    }

    @Test
    @Order(105)
    fun `conflict - create AlgorithmicStrategy - same name`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {it["exchange"] = exchangeAccount!!.id }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(106)
    fun `conflict - create AlgorithmicStrategy - same name lowerCase`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = (it["name"] as String).toUpperCase() }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(107)
    fun `conflict - create AlgorithmicStrategy - same name upperCase`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = (it["name"] as String).toUpperCase() }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(108)
    fun `conflict - update AlgorithmicStrategy - name doesn't exists`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = "TDD is Life TDD is Love" }))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(109)
    fun `notFound - update AlgorithmicStrategy - exchange doesn't exists`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {it["exchange"] = ObjectId() }))
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(110)
    fun `notFound - update AlgorithmicStrategy - strategy notFound`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${ObjectId()}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest))
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(112)
    fun `badRequest - update AlgorithmicStrategy - name is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(113)
    fun `badRequest - update AlgorithmicStrategy - name is isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(114)
    fun `badRequest - update AlgorithmicStrategy - name is isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(115)
    fun `badRequest - update AlgorithmicStrategy - name is removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("name") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(116)
    fun `badRequest - update AlgorithmicStrategy - name too short`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["exchange"] = exchangeAccount!!.id
                it["name"] = STRING_3_CHAR
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(117)
    fun `badRequest - update AlgorithmicStrategy - name too long`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["exchange"] = exchangeAccount!!.id
                it["name"] = STRING_26_CHAR
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(118)
    fun `badRequest - update AlgorithmicStrategy - description too long`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["exchange"] = exchangeAccount!!.id
                it["description"] = STRING_200_PLUS_CHAR
            }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(119)
    fun `badRequest - update AlgorithmicStrategy - description is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["description"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(120)
    fun `badRequest - update AlgorithmicStrategy - description is removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("description") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(121)
    fun `badRequest - update AlgorithmicStrategy - source too long`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = STRING_4000_PLUS_CHAR }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(122)
    fun `badRequest - update AlgorithmicStrategy - source is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(123)
    fun `badRequest - update AlgorithmicStrategy - source is isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(124)
    fun `badRequest - update AlgorithmicStrategy - source is isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["source"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(125)
    fun `badRequest - update AlgorithmicStrategy - source is removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("source") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(126)
    fun `notFound - update AlgorithmicStrategy - exchange notFound`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["exchange"] = ObjectId().toString()
            }))
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(127)
    fun `badRequest - update AlgorithmicStrategy - exchange is not ObjectId`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = "123456" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(128)
    fun `badRequest - update AlgorithmicStrategy - exchange is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(129)
    fun `badRequest - update AlgorithmicStrategy - exchange is isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(130)
    fun `badRequest - update AlgorithmicStrategy - exchange is isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(131)
    fun `badRequest - update AlgorithmicStrategy - exchange is removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("exchange") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(132)
    fun `badRequest - update AlgorithmicStrategy - configuration is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(133)
    fun `badRequest - update AlgorithmicStrategy - configuration is isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(134)
    fun `badRequest - update AlgorithmicStrategy - configuration wrong object`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = "configuration" }))
            .exchange().expectStatus().isBadRequest
    }


    @Test
    @Order(135)
    fun `badRequest - update AlgorithmicStrategy - configuration is isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["configuration"] = " " }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(136)
    fun `badRequest - update AlgorithmicStrategy - configuration is removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it.remove("configuration") }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(137)
    fun `badRequest - update AlgorithmicStrategy - quoteCurrency is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["quoteCurrency"] = null }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(138)
    fun `badRequest - update AlgorithmicStrategy - quoteCurrency is isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["quoteCurrency"] = "" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(139)
    fun `badRequest - update AlgorithmicStrategy - quoteCurrency is isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["quoteCurrency"] = " " }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(140)
    fun `badRequest - update AlgorithmicStrategy - quoteCurrency is removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it.remove("quoteCurrency") }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(141)
    fun `badRequest - update AlgorithmicStrategy - currencyList list is empty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = listOf<String>()}}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(142)
    fun `badRequest - update AlgorithmicStrategy - currencyList is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = null }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(143)
    fun `badRequest - update AlgorithmicStrategy - currencyList isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = "" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(144)
    fun `badRequest - update AlgorithmicStrategy - currencyList isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = " " }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(145)
    fun `badRequest - update AlgorithmicStrategy - currencyList removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it.remove("currencies") }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(146)
    fun `badRequest - update AlgorithmicStrategy - currencyList wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["currencies"] = "abcde" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(147)
    fun `badRequest - update AlgorithmicStrategy - timeFrame is null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = null }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(148)
    fun `badRequest - update AlgorithmicStrategy - timeFrame is isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = "" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(149)
    fun `badRequest - update AlgorithmicStrategy - timeFrame is isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = " " }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(150)
    fun `badRequest - update AlgorithmicStrategy - timeFrame is wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it["timeFrame"] = "abcde" }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(151)
    fun `badRequest - update AlgorithmicStrategy - timeFrame is removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {
                it["configuration"] = (it["configuration"] as Map<String, Any?>).copy { it.remove("timeFrame") }}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(152)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration.copy { it["longConfiguration"] = "MYTYPE" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(153)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration.copy { it["longConfiguration"] = null }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(154)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration.copy { it["longConfiguration"] = "" }))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(155)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration.copy { it.remove("longConfiguration")}))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(156)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_orderType wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["orderType"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(157)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_orderType isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["orderType"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(158)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_orderType isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["orderType"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(159)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_stopLossPercent wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["stopLossPercent"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(160)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_stopLossPercent isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["stopLossPercent"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(161)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_stopLossPercent isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["stopLossPercent"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(162)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_takeProfitPercent wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["takeProfitPercent"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(163)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_takeProfitPercent isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["takeProfitPercent"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(164)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_takeProfitPercent isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["takeProfitPercent"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(165)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_limitOffset wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["limitOffset"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(166)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_limitOffset isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["limitOffset"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(167)
    fun `badRequest - update AlgorithmicStrategy - longConfiguration_limitOffset isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("longConfiguration", jsonLongTradingConfiguration.copy { it["limitOffset"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(169)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", "MYTYPE")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(170)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", null)))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(171)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(172)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfigurationRemoveValue("shortConfiguration")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(173)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_orderType wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["orderType"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(174)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_orderType isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["orderType"] = "" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(175)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_orderType isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["orderType"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(176)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_stopLossPercent wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["stopLossPercent"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(177)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_stopLossPercent isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["stopLossPercent"] = "" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(178)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_stopLossPercent isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["stopLossPercent"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(179)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_takeProfitPercent wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["takeProfitPercent"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(180)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_takeProfitPercent isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["takeProfitPercent"] = "" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(181)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_takeProfitPercent isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["takeProfitPercent"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(182)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_limitOffset wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["limitOffset"] = "MYTYPE" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(183)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_limitOffset isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["limitOffset"] = "" })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(184)
    fun `badRequest - update AlgorithmicStrategy - shortConfiguration_limitOffset isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("shortConfiguration", jsonShortTradingConfiguration.copy { it["limitOffset"] = " " })))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(185)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", "MYTYPE")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(186)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", null)))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(187)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(188)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfigurationRemoveValue("tradeConfiguration")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(189)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_minimumQuantity wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(190)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_minimumQuantity isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(191)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_minimumQuantity isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(192)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_minimumQuantity null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["minimumQuantity"] = null }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(193)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_minimumQuantity removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it.remove("minimumQuantity") }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(194)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumQuantity wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(195)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumQuantity isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(196)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumQuantity isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(197)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumQuantity null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumQuantity"] = null }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(198)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumQuantity removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it.remove("maximumQuantity") }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(199)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_amendStakeAmount wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(200)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_amendStakeAmount isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(201)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_amendStakeAmount isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(202)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_amendStakeAmount null`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["amendStakeAmount"] = null }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(203)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_amendStakeAmount removed`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it.remove("amendStakeAmount") }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(204)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumOrderPerCoin wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOrderPerCoin"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(205)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumOrderPerCoin isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOrderPerCoin"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(206)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumOrderPerCoin isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOrderPerCoin"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(207)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumOpenOrders wrong type`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOpenOrders"] = "MYTYPE" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(208)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumOpenOrders isEmpty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOpenOrders"] = "" }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(209)
    fun `badRequest - update AlgorithmicStrategy - tradeConfiguration_maximumOpenOrders isBlank`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonTradingConfiguration("tradeConfiguration", jsonTradeConfiguration.copy { it["maximumOpenOrders"] = " " }
            ))).exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(210)
    fun `isOk - update AlgorithmicStrategy - all good params`() {
        val requestBody = SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategy::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["name"], responseBody?.name)
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["configuration"], dataClassToMap(responseBody?.configuration))
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["exchange"], responseBody?.exchange?.toString())
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["description"], responseBody?.description)
        Assertions.assertEquals(jsonAlgorithmicStrategyRequest["source"], responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
    }

    @Test
    @Order(211)
    fun `isOk - getAll Strategy - isNotEmpty`() {
        val requestBody = SharedMemory.client.get().uri("${ENDPOINT}/")
            .headers { it.setBearerAuth(SharedMemory.token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(StrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
    }

    @Test
    @Order(212)
    fun `isOk - get Strategy - isNotEmpty`() {
        val requestBody = SharedMemory.client.get().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(StrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
    }

    @Test
    @Order(213)
    fun `notFound - get Strategy - wrong ID`() {
        SharedMemory.client.get().uri("${ENDPOINT}/${ObjectId()}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(214)
    fun `badRequest - get Strategy - id not ObjectId`() {
        SharedMemory.client.get().uri("${ENDPOINT}/12456")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest

        SharedMemory.client.get().uri("${ENDPOINT}/abcds")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(215)
    fun `notFound - delete Strategy - wrong ID`() {
        SharedMemory.client.delete().uri("${ENDPOINT}/${ObjectId()}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(216)
    fun `badRequest - delete Strategy - id not ObjectId`() {
        SharedMemory.client.delete().uri("${ENDPOINT}/12346")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest

        SharedMemory.client.delete().uri("${ENDPOINT}/abcdef")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(217)
    fun `isOk - delete Strategy`() {
        val requestBody = SharedMemory.client.delete().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.success)
    }

    /**
     * NO BEARER REQUEST PART
     */

    @Test
    @Order(218)
    fun `unauthorized - noBearer`() {
        println("---\nGet All\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/")
            .exchange().expectStatus().isUnauthorized

        println("---\nGet By Id\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .exchange().expectStatus().isUnauthorized

        println("---\nCreate\n----")
        SharedMemory.client.post().uri("${ALGO_ENDPOINT}/")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy {it["exchange"] = exchangeAccount!!.id }))
            .exchange().expectStatus().isUnauthorized

        println("---\nUpdate\n----")
        SharedMemory.client.put().uri("${ALGO_ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = "Couldn't change" }))
            .exchange().expectStatus().isUnauthorized

        println("---\nDelete by Id\n----")
        SharedMemory.client.delete().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .exchange().expectStatus().isUnauthorized
    }

    /**
     * UNCONFIRMED ACCOUNT PART
     */

    @Test
    @Order(219)
    fun unconfirmCurrentAccount() = confirmCurrentAccount(false)

    @Test
    @Order(220)
    fun `unauthorized - account unconfirmed`() {
        println("---\nGet All\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

        println("---\nGet By Id\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

        println("---\nCreate\n----")
        SharedMemory.client.post().uri("${ALGO_ENDPOINT}/")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["exchange"] = exchangeAccount!!.id }))
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

        println("---\nUpdate\n----")
        SharedMemory.client.put().uri("${ALGO_ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .contentType(MediaType.APPLICATION_JSON)
            .body(bodyInserter(jsonAlgorithmicStrategyRequest.copy { it["name"] = "Couldn't change" }))
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

            println("---\nDelete by Id\n----")
            SharedMemory.client.delete().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
                .headers { it.setBearerAuth(SharedMemory.token) }
                .exchange().expectStatus().isUnauthorized
        }

}