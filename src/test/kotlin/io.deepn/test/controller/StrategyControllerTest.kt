package io.deepn.test.controller

import io.deepn.ClassOrder
import io.deepn.CommonFunction
import io.deepn.CommonFunction.confirmCurrentAccount
import io.deepn.CommonFunction.createAccountAndTest
import io.deepn.CommonFunction.getOrCreateExchangeAccount
import io.deepn.CommonFunction.deleteAllExchangeAccount
import io.deepn.CommonFunction.deleteAllStrategy
import io.deepn.SharedMemory
import io.deepn.SharedMemory.STRING_200_PLUS_CHAR
import io.deepn.SharedMemory.STRING_26_CHAR
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.STRING_4000_PLUS_CHAR
import io.deepn.SharedMemory.algorithmicStrategyResponse
import io.deepn.SharedMemory.exchangeAccount
import io.deepn.SharedMemory.tradingConfiguration
import io.deepn.common.model.BotStatus
import io.deepn.common.model.StrategyType
import io.deepn.entity.AlgorithmicStrategyRequestTest
import io.deepn.entity.AlgorithmicStrategyTest
import io.deepn.entity.StrategyTest
import io.deepn.entity.response.Ok
import io.deepn.entity.strategy.Strategy
import io.deepn.entity.strategy.impl.AlgorithmicStrategy
import org.bson.types.ObjectId
import org.junit.jupiter.api.*
import org.springframework.http.HttpStatus

@ClassOrder(4)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class StrategyControllerTest {

    companion object {
        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            createAccountAndTest()
            deleteAllExchangeAccount()
            deleteAllStrategy()
            exchangeAccount = getOrCreateExchangeAccount()
            confirmCurrentAccount()
        }

        @AfterAll
        @JvmStatic
        internal fun afterAll() {
            deleteAllExchangeAccount()
            deleteAllStrategy()
        }

        const val ENDPOINT = "/strategy"
        const val ALGO_ENDPOINT = "$ENDPOINT/algorithmic"
        val algorithmicStrategyRequest by lazy {
            AlgorithmicStrategyRequestTest(
                "My Script",
                "Description of my script",
                "a = 'ok'",
                exchangeAccount!!.id,
                tradingConfiguration
            )
        }
    }

    /**
     * CONFIRMED ACCOUNT PART
     */

    @Test
    @Order(1)
    fun `isOk - getAll Strategy - isEmpty`() {
        val requestBody = SharedMemory.client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(Strategy::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isEmpty())
    }

    /**
     * ALGORITHMIC
     */

    @Test
    @Order(2)
    fun `notFound - create AlgorithmicStrategy - exchange notFound`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(exchange = ObjectId()))
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(3)
    fun `badRequest - create AlgorithmicStrategy - currencyList is empty`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(configuration = tradingConfiguration.copy(currencies = listOf())))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - create AlgorithmicStrategy - name too short`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(name = STRING_3_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - create AlgorithmicStrategy - name too long`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(name = STRING_26_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - create AlgorithmicStrategy - description too long`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(description = STRING_200_PLUS_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - create AlgorithmicStrategy - source too long`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(source = STRING_4000_PLUS_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `isCreated - create AlgorithmicStrategy - all good params`() {
        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(algorithmicStrategyRequest.name, responseBody?.name)
        Assertions.assertEquals(algorithmicStrategyRequest.configuration.toTradingConfiguration(), responseBody?.configuration)
        Assertions.assertEquals(algorithmicStrategyRequest.exchange, responseBody?.exchange)
        Assertions.assertEquals(algorithmicStrategyRequest.description, responseBody?.description)
        Assertions.assertEquals(algorithmicStrategyRequest.source, responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
        algorithmicStrategyResponse = responseBody
    }

    @Test
    @Order(9)
    fun `isCreated - create AlgorithmicStrategy - all good params name in hebrew`() {
        val request = algorithmicStrategyRequest.copy(name = "האלפבית העברי")

        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(request)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request.name, responseBody?.name)
        Assertions.assertEquals(request.configuration.toTradingConfiguration(), responseBody?.configuration)
        Assertions.assertEquals(request.exchange, responseBody?.exchange)
        Assertions.assertEquals(request.description, responseBody?.description)
        Assertions.assertEquals(request.source, responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
        algorithmicStrategyResponse = responseBody
    }

    @Test
    @Order(10)
    fun `isCreated - create AlgorithmicStrategy - all good params name with special character1`() {
        val request = algorithmicStrategyRequest.copy(name = "=+-&,;:`£ù%^*€\$\"'(§è!çà)")
        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(request)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request.name, responseBody?.name)
        Assertions.assertEquals(request.configuration.toTradingConfiguration(), responseBody?.configuration)
        Assertions.assertEquals(request.exchange, responseBody?.exchange)
        Assertions.assertEquals(request.description, responseBody?.description)
        Assertions.assertEquals(request.source, responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
        algorithmicStrategyResponse = responseBody
    }

    @Test
    @Order(11)
    fun `isCreated - create AlgorithmicStrategy - all good params name with special character2`() {
        val request = algorithmicStrategyRequest.copy(name = "\uF8FFë“‘{¶«¡Çø}—••😀")
        val requestBody = SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(request)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request.name, responseBody?.name)
        Assertions.assertEquals(request.configuration.toTradingConfiguration(), responseBody?.configuration)
        Assertions.assertEquals(request.exchange, responseBody?.exchange)
        Assertions.assertEquals(request.description, responseBody?.description)
        Assertions.assertEquals(request.source, responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
        algorithmicStrategyResponse = responseBody
    }

    @Test
    @Order(12)
    fun `conflict - create AlgorithmicStrategy - same name`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest)
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(13)
    fun `conflict - create AlgorithmicStrategy - same name lowerCase`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(name = algorithmicStrategyRequest.name.toLowerCase()))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(14)
    fun `conflict - create AlgorithmicStrategy - same name upperCase`() {
        SharedMemory.client.post().uri("$ALGO_ENDPOINT/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(name = algorithmicStrategyRequest.name.toUpperCase()))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(15)
    fun `conflict - update AlgorithmicStrategy - name doesn't exists`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(name = "TDD is Life TDD is Love"))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(16)
    fun `notFound - update AlgorithmicStrategy - exchange doesn't exists`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(exchange = ObjectId()))
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(17)
    fun `badRequest - update AlgorithmicStrategy - currencyList is empty`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest.copy(
                configuration = algorithmicStrategyRequest.configuration.copy(currencies = emptyList())
            ))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(18)
    fun `notFound - update AlgorithmicStrategy - strategy notFound`() {
        SharedMemory.client.put().uri("$ALGO_ENDPOINT/${ObjectId()}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(algorithmicStrategyRequest)
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(20)
    fun `isOk - update AlgorithmicStrategy - all good params`() {
        val request = algorithmicStrategyRequest
        val requestBody = SharedMemory.client.put().uri("$ALGO_ENDPOINT/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .bodyValue(request)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AlgorithmicStrategy::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(SharedMemory.accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request.name, responseBody?.name)
        Assertions.assertEquals(request.configuration.toTradingConfiguration(), responseBody?.configuration)
        Assertions.assertEquals(request.exchange, responseBody?.exchange)
        Assertions.assertEquals(request.description, responseBody?.description)
        Assertions.assertEquals(request.source, responseBody?.source)
        Assertions.assertEquals(StrategyType.ALGORITHMIC, responseBody?.type)
        Assertions.assertEquals(BotStatus.STOPPED, responseBody?.status)
    }

    @Test
    @Order(21)
    fun `isOk - getAll Strategy - isNotEmpty`() {
        val requestBody = SharedMemory.client.get().uri("${ENDPOINT}/")
            .headers { it.setBearerAuth(SharedMemory.token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(StrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
    }

    @Test
    @Order(22)
    fun `isOk - get Strategy - isNotEmpty`() {
        val requestBody = SharedMemory.client.get().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(StrategyTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
    }

    @Test
    @Order(23)
    fun `notFound - get Strategy - wrong ID`() {
        SharedMemory.client.get().uri("${ENDPOINT}/${ObjectId()}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(24)
    fun `badRequest - get Strategy - id not ObjectId`() {
        SharedMemory.client.get().uri("${ENDPOINT}/12456")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest

        SharedMemory.client.get().uri("${ENDPOINT}/abcds")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(25)
    fun `notFound - delete Strategy - wrong ID`() {
        SharedMemory.client.delete().uri("${ENDPOINT}/${ObjectId()}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(26)
    fun `badRequest - delete Strategy - id not ObjectId`() {
        SharedMemory.client.delete().uri("${ENDPOINT}/12346")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest

        SharedMemory.client.delete().uri("${ENDPOINT}/abcdef")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(27)
    fun `isOk - delete Strategy`() {
        val requestBody = SharedMemory.client.delete().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.success)
    }

    /**
     * NO BEARER REQUEST PART
     */

    @Test
    @Order(28)
    fun `unauthorized - noBearer`() {
        println("---\nGet All\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/")
            .exchange().expectStatus().isUnauthorized

        println("---\nGet By Id\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .exchange().expectStatus().isUnauthorized

        println("---\nCreate\n----")
        SharedMemory.client.post().uri("${ALGO_ENDPOINT}/")
            .bodyValue(algorithmicStrategyRequest)
            .exchange().expectStatus().isUnauthorized

        println("---\nUpdate\n----")
        SharedMemory.client.put().uri("${ALGO_ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .bodyValue(algorithmicStrategyRequest.copy(name = "Couldn't change"))
            .exchange().expectStatus().isUnauthorized

        println("---\nDelete by Id\n----")
        SharedMemory.client.delete().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .exchange().expectStatus().isUnauthorized
    }

    /**
     * UNCONFIRMED ACCOUNT PART
     */

    @Test
    @Order(29)
    fun unconfirmCurrentAccount() = confirmCurrentAccount(false)

    @Test
    @Order(30)
    fun `unauthorized - account unconfirmed`() {
        println("---\nGet All\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

        println("---\nGet By Id\n----")
        SharedMemory.client.get().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

        println("---\nCreate\n----")
        SharedMemory.client.post().uri("${ALGO_ENDPOINT}/")
            .bodyValue(algorithmicStrategyRequest)
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

        println("---\nUpdate\n----")
        SharedMemory.client.put().uri("${ALGO_ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .bodyValue(algorithmicStrategyRequest.copy(name = "Couldn't change"))
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized

        println("---\nDelete by Id\n----")
        SharedMemory.client.delete().uri("${ENDPOINT}/${algorithmicStrategyResponse?.id}")
            .headers { it.setBearerAuth(SharedMemory.token) }
            .exchange().expectStatus().isUnauthorized
    }

}