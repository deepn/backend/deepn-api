package io.deepn.test.controller

import io.deepn.ClassOrder
import io.deepn.CommonFunction
import io.deepn.CommonFunction.confirmCurrentAccount
import io.deepn.CommonFunction.createAccountAndTest
import io.deepn.CommonFunction.deleteAllExchangeAccount
import io.deepn.SharedMemory.STRING_26_CHAR
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.accountResponse
import io.deepn.SharedMemory.client
import io.deepn.SharedMemory.exchangeAccount
import io.deepn.SharedMemory.exchangeRequest
import io.deepn.SharedMemory.token
import io.deepn.common.model.Exchange
import io.deepn.entity.ExchangeAccount
import io.deepn.entity.request.ExchangeAccountRequest
import io.deepn.entity.request.ExchangeCredentialsRequest
import org.bson.types.ObjectId
import org.junit.jupiter.api.*
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

@ClassOrder(3)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class ExchangeAccountControllerTest {

    companion object {
        private const val ENDPOINT = "/exchange-account"
        var exchangeAccount2: ExchangeAccount? = null

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            createAccountAndTest()
            confirmCurrentAccount()
        }

        @AfterAll
        @JvmStatic
        fun deleteAccount() {
            deleteAllExchangeAccount()
        }
    }


    @Test
    @Order(1)
    fun `isOk - getAll Exchange`() {
        val requestBody = client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isEmpty())
    }

    @Test
    @Order(2)
    fun `isOk - get Exchange - empty`() {
        val requestBody = client.get().uri("$ENDPOINT/${Exchange.BINANCE}")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isEmpty())
    }

    @Test
    @Order(3)
    fun `badRequest - get Exchange - exchange doesn't exist`() {
        client.get().uri("$ENDPOINT/DEEPN")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - create Exchange - name too short`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(name = STRING_3_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - create Exchange - name too long`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(name = STRING_26_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - create Exchange - credentialKey isEmpty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(credentials = ExchangeCredentialsRequest(
                "",
                exchangeRequest.credentials.secret,
                null
            )))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - create Exchange - credentialSecret isEmpty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(credentials = ExchangeCredentialsRequest(
                exchangeRequest.credentials.key,
                "",
                null
            )))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `badRequest - create Exchange - credentialPassphrase isEmpty`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(credentials = ExchangeCredentialsRequest(
                exchangeRequest.credentials.key,
                exchangeRequest.credentials.secret,
                ""
            )))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(9)
    fun `isCreated - create Exchange - all good parameters`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(exchangeRequest.name, responseBody?.name)
        Assertions.assertEquals(exchangeRequest.exchange, responseBody?.exchange)
        Assertions.assertEquals(exchangeRequest.credentials.toExchangeCredentials(), responseBody?.credentials)
        exchangeAccount = responseBody
    }

    @Order(10)
    @Test
    fun `conflict - create Exchange - already exists`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest)

        requestBody.exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(11)
    fun `conflict - create Exchange - name in lower case already exists`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(name = exchangeRequest.name.toLowerCase()))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(12)
    fun `conflict - create Exchange - name in upper case already exists`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(name = exchangeRequest.name.toUpperCase()))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(13)
    fun `conflict - create Exchange - name already exists different exchange`() {
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(exchange = Exchange.COINBASE))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(14)
    fun `isOk - getAllExchange - isNotEmpty`() {
        val requestBody = client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
        val exchange = responseBody?.first()
        Assertions.assertEquals(exchangeAccount, exchange)
    }

    @Test
    @Order(15)
    fun `isOk - get Exchange BINANCE`() {
        val requestBody = client.get().uri("$ENDPOINT/${Exchange.BINANCE}")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        val exchange = responseBody?.first()
        Assertions.assertEquals(exchangeAccount, exchange)
    }

    @Test
    @Order(16)
    fun `isCreated - create Exchange2 - all good parameters`() {
        val request = ExchangeAccountRequest(
            "Exchange2",
            Exchange.FTX,
            ExchangeCredentialsRequest("key2", "secret2", "passphrase2")
        )
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(request)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(request.name, responseBody?.name)
        Assertions.assertEquals(request.exchange, responseBody?.exchange)
        Assertions.assertEquals(request.credentials.toExchangeCredentials(), responseBody?.credentials)
        exchangeAccount2 = responseBody
    }

    @Test
    @Order(17)
    fun `isOk - updateExchange - different name`() {
        val requestBody = client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(exchangeRequest.copy(name = "MyName"))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals("MyName", responseBody?.name)
        Assertions.assertEquals(Exchange.BINANCE, responseBody?.exchange)
        Assertions.assertEquals(exchangeRequest.credentials.toExchangeCredentials(), responseBody?.credentials)
        exchangeAccount = responseBody
    }

    @Test
    @Order(18)
    fun `isOk - updateExchange - different exchange`() {
        val requestBody = client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(exchangeRequest.copy(exchange = Exchange.FTX))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(exchangeRequest.name, responseBody?.name)
        Assertions.assertEquals(Exchange.FTX, responseBody?.exchange)
        Assertions.assertEquals(exchangeRequest.credentials.toExchangeCredentials(), responseBody?.credentials)
        exchangeAccount = responseBody
    }

    @Test
    @Order(19)
    fun `isOk - getAllExchange - 2 elements`() {
        val requestBody = client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
        Assertions.assertEquals(2, responseBody?.size)
        val exchange1 = responseBody?.first()
        Assertions.assertEquals(exchangeAccount, exchange1)
        val exchange2 = responseBody?.get(1)
        Assertions.assertEquals(exchangeAccount2, exchange2)
    }

    @Test
    @Order(20)
    fun `isOk - get Exchange FTX - 2 elements`() {
        val requestBody = client.get().uri("$ENDPOINT/${Exchange.FTX}")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBodyList(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.isNotEmpty())
        Assertions.assertEquals(2, responseBody?.size)
        val exchange1 = responseBody?.first()
        Assertions.assertEquals(exchangeAccount, exchange1)
        val exchange2 = responseBody?.get(1)
        Assertions.assertEquals(exchangeAccount2, exchange2)
    }

    @Test
    @Order(21)
    fun `badRequest - update Exchange - name too short`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(name = STRING_3_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(22)
    fun `badRequest - update Exchange - name too long`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(name = STRING_26_CHAR))
            .exchange().expectStatus().isBadRequest
    }


    @Test
    @Order(23)
    fun `badRequest - update Exchange - credentialKey isEmpty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(credentials = exchangeRequest.credentials.copy(key = "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(24)
    fun `badRequest - update Exchange - credentialSecret isEmpty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(credentials = exchangeRequest.credentials.copy(secret = "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(25)
    fun `badRequest - update Exchange - credentialPassphrase isEmpty`() {
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest.copy(credentials = exchangeRequest.credentials.copy(passphrase = "")))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(26)
    fun `badRequest - update Exchange - wrong ID`() {
        client.put().uri("${ENDPOINT}/${ObjectId()}")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest)
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(27)
    fun `badRequest - update Exchange - ID isn't ObjectId`() {
        client.put().uri("${ENDPOINT}/${exchangeAccount?.id}12")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest)
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(28)
    fun `badRequest - delete Exchange - wrong ID`() {
        client.delete().uri("$ENDPOINT/${ObjectId()}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isNotFound
    }

    @Test
    @Order(29)
    fun `badRequest - delete Exchange - ID isn't ObjectId`() {
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}12")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(30)
    fun `isOk - delete Exchange - good ID`() {
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isOk
    }

    @Test
    @Order(31)
    fun `notFound - delete Exchange - already Deleted`() {
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isNotFound
    }

    /**
     * NO BEARER REQUEST PART
     */

    @Test
    @Order(32)
    fun `isCreated - create Exchange - to make sure we don't have access`() {
        val requestBody = client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(ExchangeAccount::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertNotNull(responseBody)
        Assertions.assertEquals(accountResponse?.id, responseBody?.owner)
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(CommonFunction.isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        Assertions.assertEquals(exchangeRequest.name, responseBody?.name)
        Assertions.assertEquals(exchangeRequest.exchange, responseBody?.exchange)
        Assertions.assertEquals(exchangeRequest.credentials.toExchangeCredentials(), responseBody?.credentials)
        exchangeAccount = responseBody
    }

    @Test
    @Order(33)
    fun `unauthorized - account no bearer`() {
        println("----\nGetAll\n----")
        client.get().uri("$ENDPOINT/")
            .exchange().expectStatus().isUnauthorized

        println("----\nGet By Exchange\n----")
        client.get().uri("$ENDPOINT/${Exchange.BINANCE}")
            .exchange().expectStatus().isUnauthorized

        println("----\nCreate Exchange\n----")
        client.post().uri("$ENDPOINT/")
            .bodyValue(exchangeRequest)
            .exchange().expectStatus().isUnauthorized

        println("----\nUpdate Exchange\n----")
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .bodyValue(exchangeRequest)
            .exchange().expectStatus().isUnauthorized

        println("----\nDelete Exchange\n----")
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .exchange().expectStatus().isUnauthorized
    }


    /**
     * UNCONFIRMED ACCOUNT PART
     */

    @Test
    @Order(34)
    fun unconfirmCurrentAccount() = confirmCurrentAccount(false)

    @Test
    @Order(35)
    fun `unauthorized - account not confirmed`() {
        println("----\nGetAll\n----")
        client.get().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isUnauthorized

        println("----\nGet By Exchange\n----")
        client.get().uri("$ENDPOINT/${Exchange.BINANCE}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isUnauthorized

        println("----\nCreate Exchange\n----")
        client.post().uri("$ENDPOINT/")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest)
            .exchange().expectStatus().isUnauthorized

        println("----\nUpdate Exchange\n----")
        client.put().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .bodyValue(exchangeRequest)
            .exchange().expectStatus().isUnauthorized

        println("----\nDelete Exchange\n----")
        client.delete().uri("$ENDPOINT/${exchangeAccount?.id}")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isUnauthorized
    }
}