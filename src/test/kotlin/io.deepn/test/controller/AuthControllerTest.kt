package io.deepn.test.controller

import io.deepn.ClassOrder
import io.deepn.CommonFunction.deleteAllAccount
import io.deepn.CommonFunction.isLessThanAMinute
import io.deepn.SharedMemory.STRING_26_CHAR
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.STRING_61_CHAR
import io.deepn.SharedMemory.STRING_7_CHAR
import io.deepn.SharedMemory.accountRequest
import io.deepn.SharedMemory.client
import io.deepn.SharedMemory.loginRequest
import io.deepn.entity.AccountRole
import io.deepn.entity.LoginResponseTest
import io.deepn.entity.response.Ok
import org.junit.jupiter.api.*
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType

@ClassOrder(1)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class AuthControllerTest {

    companion object {
        const val ENDPOINT = "auth"

        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            deleteAllAccount()
        }

    }

    @Test
    @Order(1)
    fun `isOk - emailAvailable - true`() {
        val requestBody = client.get().uri("$ENDPOINT/available/email?email=${accountRequest.email}")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.success)
    }

    @Test
    @Order(2)
    fun `isOk - usernameAvailable - true`() {
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=${accountRequest.username}")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.success)
    }

    @Test
    @Order(3)
    fun `badRequest - createAccount - email is number`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(email = "1234567"))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - createAccount - email isn't an email`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(email = "bobo@.co"))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - createAccount - email isEmpty`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(email = ""))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - createAccount - email hasSpace`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(email = "bo bo @gmail"))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - createAccount - username too short`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(username = STRING_3_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `badRequest - createAccount - username too long`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(username = STRING_26_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(9)
    fun `badRequest - createAccount - username has special character`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(username = "abcd$"))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(10)
    fun `badRequest - createAccount - username has space`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(username = "abcd azefg"))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(11)
    fun `badRequest - createAccount - username isEmpty`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(username = ""))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(12)
    fun `badRequest - createAccount - password too short`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(password = STRING_7_CHAR))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(13)
    fun `badRequest - createAccount - password too long`() {
        client.post().uri("$ENDPOINT/register")
            .bodyValue(accountRequest.copy(password = STRING_61_CHAR))
            .exchange().expectStatus().isBadRequest
    }


    @Test
    @Order(14)
    fun `isCreated - createAccount`() {
        val requestBody = client.post().uri("$ENDPOINT/register").bodyValue(accountRequest)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java)
            .returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertEquals(accountRequest.username, responseBody?.account?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
    }

    @Test
    @Order(15)
    fun `conflict - createAccount - already exists`() {
        client.post().uri("$ENDPOINT/register").bodyValue(accountRequest)
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(16)
    fun `conflict - createAccount - email used but available username`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(accountRequest.copy(username = "B0b0tan"))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(17)
    fun `conflict - createAccount - username used but available email`() {
        client.post().uri("$ENDPOINT/register")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(accountRequest.copy(email = "B0b0t@dc.com"))
            .exchange().expectStatus().isEqualTo(HttpStatus.CONFLICT)
    }

    @Test
    @Order(18)
    fun `badRequest loginAccount - email wrongly formatted`() {
        client.post().uri("$ENDPOINT/login")
            .bodyValue(loginRequest.copy(login = "bobo"))
            .exchange().expectStatus().isBadRequest
    }


    @Test
    @Order(19)
    fun `unauthorized - loginAccount - email doesn't exist`() {
        client.post().uri("$ENDPOINT/login")
            .bodyValue(loginRequest.copy(login = "te@test.fr"))
            .exchange().expectStatus().isUnauthorized
    }

    @Test
    @Order(20)
    fun `unauthorized - loginAccount - wrong password`() {
        client.post().uri("$ENDPOINT/login")
            .bodyValue(loginRequest.copy(password = "azerty"))
            .exchange().expectStatus().isUnauthorized
    }

    @Test
    @Order(21)
    fun `badRequest - loginAccount - email empty`() {
        client.post().uri("$ENDPOINT/login")
            .bodyValue(loginRequest.copy(login = ""))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(22)
    fun `badRequest - loginAccount - password empty`() {
        client.post().uri("$ENDPOINT/login")
            .bodyValue(loginRequest.copy(password = ""))
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(23)
    fun `isOk - loginAccount`() {
        val requestBody = client.post().uri("$ENDPOINT/login").bodyValue(loginRequest)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(accountRequest.username, responseBody?.account?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
    }

    @Test
    @Order(24)
    fun `isOk - emailAvailable - false`() {
        val requestBody = client.get().uri("$ENDPOINT/available/email?email=${accountRequest.email}")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(25)
    fun `badRequest - emailAvailable isn't formatted`() {
        client.get().uri("$ENDPOINT/available/email?email=boboleplusbo")
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(26)
    fun `isOk - emailAvailable with space - false`() {
        val emailWithSpace = accountRequest.email.replace("..", "$0 ")
        val requestBody = client.get().uri("$ENDPOINT/available/email?email=$emailWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(27)
    fun `isOk - emailAvailable with multiple space - false`() {
        val emailWithSpace = accountRequest.email.replace("..", "$0      ")
        val requestBody = client.get().uri("$ENDPOINT/available/email?email=$emailWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(28)
    fun `isOk - emailAvailable upperCase - false`() {
        val requestBody = client.get()
            .uri("$ENDPOINT/available/email?email=${accountRequest.email.toUpperCase()}")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(29)
    fun `isOk - emailAvailable with space uppercase - false`() {
        val emailWithSpace = accountRequest.email.replace("..", "$0 ").toUpperCase()
        val requestBody = client.get().uri("$ENDPOINT/available/email?email=$emailWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(30)
    fun `isOk - emailAvailable with multiple space uppercase - false`() {
        val emailWithSpace = accountRequest.email.replace("..", "$0      ").toUpperCase()
        val requestBody = client.get().uri("$ENDPOINT/available/email?email=$emailWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(31)
    fun `badRequest - emailAvailable - empty`() {
        client.get().uri("$ENDPOINT/available/email?email=").exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(32)
    fun `badRequest - usernameAvailable - empty`() {
        client.get().uri("$ENDPOINT/available/username?username=").exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(33)
    fun `isOk - usernameAvailable - false`() {
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=${accountRequest.username}")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(34)
    fun `isOk - usernameAvailable with space - false`() {
        val usernameWithSpace = accountRequest.username.replace("..", "$0 ")
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=$usernameWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(35)
    fun `isOk - usernameAvailable with multiple space - false`() {
        val usernameWithSpace = accountRequest.username.replace("..", "$0      ")
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=$usernameWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(36)
    fun `isOk - usernameAvailable upperCase - false`() {
        val requestBody = client.get()
            .uri("$ENDPOINT/available/username?username=${accountRequest.username.toUpperCase()}")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(37)
    fun `isOk - usernameAvailable with space uppercase - false`() {
        val usernameWithSpace = accountRequest.username.replace("..", "$0 ").toUpperCase()
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=$usernameWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(38)
    fun `isOk - usernameAvailable with multiple space uppercase - false`() {
        val usernameWithSpace = accountRequest.username.replace("..", "$0      ").toUpperCase()
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=$usernameWithSpace")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(false, responseBody?.success)
    }

    @Test
    @Order(39)
    fun `isOk - usernameAvailable - with number`() {
        val username = accountRequest.username.toUpperCase()+"1254"
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=$username")
            .accept(MediaType.APPLICATION_JSON)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.success)
    }

    @Test
    @Order(40)
    fun `badRequest - usernameAvailable - email instead of username`() {
        client.get().uri("$ENDPOINT/available/username?username=${accountRequest.email}")
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(41)
    fun `isOk - usernameAvailable with only number - false`() {
        val requestBody = client.get().uri("$ENDPOINT/available/username?username=123456789")

        val response = requestBody.exchange()
        val responseBody = response.expectBody(Ok::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(true, responseBody?.success)
    }


}
