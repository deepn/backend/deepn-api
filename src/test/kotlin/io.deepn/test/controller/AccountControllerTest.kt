package io.deepn.test.controller

import io.deepn.ClassOrder
import io.deepn.CommonFunction
import io.deepn.CommonFunction.blockingGet
import io.deepn.CommonFunction.isLessThanAMinute
import io.deepn.SharedMemory.STRING_3_CHAR
import io.deepn.SharedMemory.STRING_61_CHAR
import io.deepn.SharedMemory.STRING_7_CHAR
import io.deepn.SharedMemory.accountRepository
import io.deepn.SharedMemory.accountRequest
import io.deepn.SharedMemory.client
import io.deepn.SharedMemory.loginRequest
import io.deepn.SharedMemory.token
import io.deepn.entity.AccountRole
import io.deepn.entity.AccountTest
import io.deepn.entity.LoginResponseTest
import io.deepn.entity.request.ConfirmationCode
import io.deepn.entity.request.PasswordUpdateRequest
import io.deepn.entity.response.Ok
import io.deepn.entity.response.TokenResponse
import org.bson.types.ObjectId
import org.junit.jupiter.api.*
import org.springframework.http.MediaType

@ClassOrder(2)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class AccountControllerTest {

    companion object {
        var accountId: ObjectId? = null
        var confirmationCode: String? = null
        const val ENDPOINT = "account"


        @BeforeAll
        @JvmStatic
        internal fun beforeAll() {
            CommonFunction.createAccountAndTest()
        }

    }

    @Test
    @Order(1)
    fun getAccount() {
        val requestBody = client.get().uri("$ENDPOINT/")
            .accept(MediaType.APPLICATION_JSON)
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        val responseBody = response.expectBody(AccountTest::class.java).returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(accountRequest.username, responseBody?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.email)
        Assertions.assertEquals(false, responseBody?.confirmed)
        Assertions.assertNull(responseBody?.confirmationCode)
        Assertions.assertNull(responseBody?.confirmationSendTime)
        Assertions.assertNull(responseBody?.resetToken)
        Assertions.assertNull(responseBody?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.role)
        Assertions.assertTrue(isLessThanAMinute(responseBody?.creationTime?.epochSecond))
        Assertions.assertTrue(isLessThanAMinute(responseBody?.updateTime?.epochSecond))
        accountId = responseBody!!.id
    }

    @Test
    @Order(2)
    fun `badRequest - updatePassword - current password too short`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest(STRING_3_CHAR, "STRING_3_CHAR"))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(3)
    fun `badRequest - updatePassword - current password too long`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest(STRING_61_CHAR, "STRING_61_CHAR"))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(4)
    fun `badRequest - updatePassword - current password is empty`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest("", "loginRequest.password"))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(5)
    fun `badRequest - updatePassword - new password too short`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest(loginRequest.password, STRING_3_CHAR))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(6)
    fun `badRequest - updatePassword - new password too long`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest(loginRequest.password, STRING_61_CHAR))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(7)
    fun `badRequest - updatePassword - new password is empty`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest(loginRequest.password, ""))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(8)
    fun `unauthorized - updatePassword - current password doesn't match`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest("blablabla", "newpassword"))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isUnauthorized
    }

    @Test
    @Order(9)
    fun `isOk - login - before changing password`() {
        val requestBody = client.post().uri("auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(loginRequest)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java)
            .returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(accountRequest.username, responseBody?.account?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
        token = responseBody!!.token
    }

    @Test
    @Order(10)
    fun `noContent - updatePassword - success - true`() {
        client.put().uri("$ENDPOINT/update-password")
            .bodyValue(PasswordUpdateRequest(loginRequest.password, "Holà qué tal"))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isNoContent
            .expectBody().isEmpty
    }

    @Test
    @Order(11)
    fun `isOk - login - after changing password`() {
        val requestBody = client.post().uri("auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(loginRequest.copy( password = "Holà qué tal"))

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java)
            .returnResult().responseBody

        response.expectStatus().isOk
        Assertions.assertEquals(accountRequest.username, responseBody?.account?.username)
        Assertions.assertEquals(accountRequest.email, responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertNull(responseBody?.account?.confirmationCode)
        Assertions.assertNull(responseBody?.account?.confirmationSendTime)
        Assertions.assertNull(responseBody?.account?.resetToken)
        Assertions.assertNull(responseBody?.account?.resetTokenSendTime)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.creationTime?.epochSecond))
        Assertions.assertTrue(isLessThanAMinute(responseBody?.account?.updateTime?.epochSecond))
        Assertions.assertNotNull(responseBody?.token)
        token = responseBody!!.token
    }

    @Test
    @Order(12)
    fun `unauthorized login and wrong password`() {
        client.post().uri("auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(loginRequest)
            .exchange().expectStatus().isUnauthorized
            .expectBody().isEmpty
    }

    @Test
    @Order(13)
    fun `generate confirm code and check update`() {
        Assertions.assertNotNull(accountId)
        accountRepository.findById(accountId.toString()).blockingGet().apply {
            Assertions.assertNull(this?.confirmationCode)
            Assertions.assertNull(this?.confirmationSendTime)
            Assertions.assertEquals(false, this?.confirmed)
            println("Done findById")
        }

        val requestBody = client.post().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()

        response.expectStatus().isOk
        Assertions.assertTrue(response.expectBody(Ok::class.java).returnResult().responseBody!!.success)

        accountRepository.findById(accountId.toString()).blockingGet().apply {
            Assertions.assertNotNull(this?.confirmationCode)
            Assertions.assertNotNull(this?.confirmationSendTime)
            Assertions.assertEquals(false, this?.confirmed)
            confirmationCode = this?.confirmationCode
            println("Done findById")
        }
    }

    @Test
    @Order(14)
    fun `badRequest - confirm - delay between calls too short`() {
        client.post().uri("$ENDPOINT/confirm")
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(15)
    fun `isOk - confirm code - wrong confirmationCode success false`() {
        val requestBody = client.patch().uri("$ENDPOINT/confirm")
            .bodyValue(ConfirmationCode(if(confirmationCode == "9999") "9998" else "9999"))
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        response.expectStatus().isOk
        Assertions.assertFalse(response.expectBody(Ok::class.java).returnResult().responseBody!!.success)
    }

    @Test
    @Order(16)
    fun `badRequest - confirm code - alphabet code`() {
        client.patch().uri("$ENDPOINT/confirm")
            .bodyValue(ConfirmationCode("abcd"))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isOk
            .apply {
                Assertions.assertEquals(false, expectBody(Ok::class.java).returnResult().responseBody?.success)
            }
    }

    @Test
    @Order(17)
    fun `badRequest - confirm code - alphanumeric confirmationCode`() {
        client.patch().uri("$ENDPOINT/confirm")
            .bodyValue(ConfirmationCode("ab23"))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isOk
            .apply {
                Assertions.assertEquals(false, expectBody(Ok::class.java).returnResult().responseBody?.success)
            }
    }

    @Test
    @Order(18)
    fun `badRequest - confirm code - too short`() {
        client.patch().uri("$ENDPOINT/confirm")
            .bodyValue(ConfirmationCode(STRING_3_CHAR))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(19)
    fun `badRequest - confirm code - too long`() {
        client.patch().uri("$ENDPOINT/confirm")
            .bodyValue(ConfirmationCode(STRING_7_CHAR))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(20)
    fun `badRequest - confirm code - isEmpty`() {
        client.patch().uri("$ENDPOINT/confirm")
            .bodyValue(ConfirmationCode(""))
            .headers { it.setBearerAuth(token) }
            .exchange().expectStatus().isBadRequest
    }

    @Test
    @Order(21)
    fun `isOk confirm code success true`() {
        val requestBody = client.patch().uri("$ENDPOINT/confirm")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(ConfirmationCode(confirmationCode!!))
            .headers { it.setBearerAuth(token) }

        val response = requestBody.exchange()
        response.expectStatus().isOk
        Assertions.assertTrue(response.expectBody(Ok::class.java).returnResult().responseBody!!.success)
    }

    @Test
    @Order(22)
    fun `isOk generate new token`() {
        val requestBody = client.get().uri("$ENDPOINT/newToken")
            .headers { it.setBearerAuth(token) }
        val response = requestBody.exchange()

        response.expectStatus().isOk
        val newToken = response.expectBody(TokenResponse::class.java).returnResult().responseBody!!.token

        val getAccountBody = client.get().uri("$ENDPOINT/")
            .accept(MediaType.APPLICATION_JSON)
            .headers { it.setBearerAuth(newToken) }

        getAccountBody.exchange().expectStatus().isOk
    }

    @Test
    @Order(23)
    fun `unauthorized - no bearer`() {
        client.get().uri("$ENDPOINT/newToken").exchange().expectStatus().isUnauthorized
    }

}