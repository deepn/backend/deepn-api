package io.deepn.test.entity

import io.deepn.ClassOrder
import io.deepn.entity.*
import io.deepn.entity.request.AlgorithmicStrategyRequest
import io.deepn.entity.response.LoginResponse
import io.deepn.entity.strategy.Strategy
import io.deepn.entity.strategy.impl.AlgorithmicStrategy
import org.junit.jupiter.api.*
import kotlin.reflect.KClass
import kotlin.reflect.KType
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.jvmErasure

@ClassOrder(40)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class TestEntity {

    private fun KClass<*>.memberSize(): Int = declaredMemberProperties.size

    private fun KClass<*>.getTypes(): List<KType> {
        val types = mutableListOf<KType>()
        declaredMemberProperties.map {
            if (it.returnType.jvmErasure.isData)
                types.addAll(it.returnType.jvmErasure.getTypes())
            else
                types.add(it.returnType)
        }
        return types.sortedBy { it.isMarkedNullable }.sortedBy { it.toString() }
    }

    @Test
    @Order(1)
    fun `is LoginResponseTest equals LoginResponse`() {
        // Check if same number of properties
        Assertions.assertEquals(LoginResponse::class.memberSize(), LoginResponseTest::class.memberSize())
        // Check if same number of type
        Assertions.assertEquals(LoginResponse::class.getTypes(),LoginResponseTest::class.getTypes())
    }

    @Test
    @Order(2)
    fun `is AccountTest equals Account`() {
        // Check if same number of properties
        Assertions.assertEquals(Account::class.memberSize(), AccountTest::class.memberSize())
        // Check if same number of type
        Assertions.assertEquals(Account::class.getTypes(),AccountTest::class.getTypes())
    }

    @Test
    @Order(3)
    fun `is AlgorithmicStrategyTest equals AlgorithmicStrategy`() {
        // Check if same number of properties
        Assertions.assertEquals(AlgorithmicStrategy::class.memberSize(), AlgorithmicStrategyTest::class.memberSize())
        // Check if same number of type
        Assertions.assertEquals(AlgorithmicStrategy::class.getTypes(),AlgorithmicStrategyTest::class.getTypes())
    }

    @Test
    @Order(4)
    fun `is StrategyTest equals Strategy`() {
        // Check if same number of properties
        Assertions.assertEquals(Strategy::class.memberSize(), StrategyTest::class.memberSize())
        // Check if same number of type
        Assertions.assertEquals(Strategy::class.getTypes(),StrategyTest::class.getTypes())
    }
    @Test
    @Order(5)
    fun `is AlgorithmicStrategyRequestTest equals AlgorithmicStrategyRequest`() {
        // Check if same number of properties
        Assertions.assertEquals(AlgorithmicStrategyRequest::class.memberSize(), AlgorithmicStrategyRequestTest::class.memberSize())
        // Check if same number of type
        Assertions.assertEquals(AlgorithmicStrategyRequest::class.getTypes(),AlgorithmicStrategyRequestTest::class.getTypes())
    }


}