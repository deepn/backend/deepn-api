package io.deepn

import io.deepn.SharedMemory.context
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.boot.runApplication

@ClassOrder(0)
class TestServerStarter {

    companion object {
        @BeforeAll
        @JvmStatic
        internal fun startServer() {
            context = runApplication<DeepnApplication>("--spring.profiles.active=test")
        }
    }

    @Test
    fun initialize() {
        println("Service initialized")
    }

}