package io.deepn

import com.fasterxml.jackson.core.type.TypeReference
import io.deepn.SharedMemory.accountOperationTest
import io.deepn.SharedMemory.accountRepository
import io.deepn.SharedMemory.accountResponse
import io.deepn.SharedMemory.beanConfiguration
import io.deepn.SharedMemory.exchangeAccountRepository
import io.deepn.SharedMemory.exchangeRequest
import io.deepn.SharedMemory.strategyRepository
import io.deepn.SharedMemory.token
import io.deepn.entity.AccountRole
import io.deepn.entity.ExchangeAccount
import io.deepn.entity.LoginResponseTest
import org.junit.jupiter.api.Assertions
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import reactor.core.publisher.Mono
import kotlin.math.abs

internal object CommonFunction {

    fun <T : Any> Mono<T>.blockingGet(): T? {
        return this.toFuture().get()
    }

    fun isLessThanAMinute(time: Long?): Boolean {
        time ?: return false
        return abs(time.minus(System.currentTimeMillis())) < 60000
    }

    fun dataClassToMap(dataclass: Any?): MutableMap<String, Any?> =
        beanConfiguration.objectMapper().convertValue(dataclass, object: TypeReference<MutableMap<String, Any?>>() {})

    fun Map<String, Any?>.copy(callback: (MutableMap<String, Any?>) -> Unit) =
        toMutableMap().apply { callback(this) }

    fun <T> bodyInserter(body : T) = BodyInserters.fromValue(body)

    fun createAccountAndTest() {
        deleteAllAccount()
        val requestBody = SharedMemory.client.post().uri("auth/register")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.ALL)
            .bodyValue(SharedMemory.accountRequest)

        val response = requestBody.exchange()
        val responseBody = response.expectBody(LoginResponseTest::class.java).returnResult().responseBody

        response.expectStatus().isCreated
        Assertions.assertEquals(SharedMemory.accountRequest.username, responseBody?.account?.username)
        Assertions.assertEquals(SharedMemory.accountRequest.email, responseBody?.account?.email)
        Assertions.assertEquals(false, responseBody?.account?.confirmed)
        Assertions.assertEquals(AccountRole.ROLE_USER, responseBody?.account?.role)
        Assertions.assertNotNull(responseBody?.token)
        token = responseBody!!.token
        accountResponse = responseBody.account
    }

    fun confirmCurrentAccount(confirmed: Boolean = true) {
        Assertions.assertNotNull(accountResponse)
        Assertions.assertNotNull(accountResponse?.id)
        accountOperationTest.setConfirmed(accountResponse!!.id, confirmed).blockingGet()
    }

    fun getOrCreateExchangeAccount(): ExchangeAccount {
        val exchangeAccount = exchangeRequest.toExchangeAccount(accountResponse!!.id)
        return exchangeAccountRepository.findByName(exchangeAccount.name).block()
            ?: exchangeAccountRepository.insert(exchangeAccount).block() ?: throw IllegalStateException("fail to get exchangeAccount")
    }

    fun deleteAllAccount() {
        accountRepository.deleteAll().block()
    }

    fun deleteAllStrategy() {
        strategyRepository.deleteAll().block()
    }

    fun deleteAllExchangeAccount() {
        exchangeAccountRepository.deleteAll().block()
    }

}