package io.deepn.entity

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import io.deepn.constant.FieldConstant
import io.deepn.entity.request.TradingConfigurationRequest
import io.deepn.validation.NormalizeSpace
import org.bson.types.ObjectId
import javax.validation.Valid
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class AlgorithmicStrategyRequestTest(
    @field:Size(
        min = FieldConstant.MIN_SIZE_NAME,
        max = FieldConstant.MAX_SIZE_NAME
    ) @JsonDeserialize(using = NormalizeSpace::class) val name: String,
    @field:Size(max = FieldConstant.MAX_SIZE_DESCRIPTION) val description: String,
    @field:Size(max = FieldConstant.MAX_SIZE_SOURCE) @field:NotBlank val source: String,
    @JsonSerialize(using = ToStringSerializer::class) val exchange: ObjectId,
    @field:Valid val configuration: TradingConfigurationRequest
)