package io.deepn.entity

import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import io.deepn.common.TradingConfiguration
import io.deepn.common.model.BotAddress
import io.deepn.common.model.BotStatus
import io.deepn.common.model.StrategyType
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Field
import java.time.Instant

data class LoginResponseTest(val account: AccountTest, val token: String)

data class AccountTest(
    @JsonSerialize(using = ToStringSerializer::class)
    val id: ObjectId,

    val username: String,

    val email: String,

    @JsonIgnoreProperties
    val password: String = "",

    @JsonProperty("confirmation.code")
    val confirmationCode: String? = null,

    @JsonProperty("confirmation.sendTime")
    val confirmationSendTime: Instant?,

    @JsonProperty("confirmation.confirmed")
    val confirmed: Boolean,

    @JsonProperty("time.creation")
    val creationTime: Instant,

    @JsonProperty("time.update")
    val updateTime: Instant,

    val resetToken: String? = null,

    val resetTokenSendTime: Instant? = null,

    val role: AccountRole = AccountRole.ROLE_USER,
)

@JsonTypeName("ALGORITHMIC")
class AlgorithmicStrategyTest(
    id: ObjectId = ObjectId.get(),
    name: String,
    description: String,
    owner: ObjectId,
    type: StrategyType = StrategyType.ALGORITHMIC,
    exchange: ObjectId,
    configuration: TradingConfiguration,
    status: BotStatus = BotStatus.STOPPED,
    address: BotAddress? = null,
    creationTime: Instant = Instant.now(),
    updateTime: Instant = Instant.now(),
    val source: String
) : StrategyTest(
    id,
    name,
    description,
    owner,
    type,
    exchange,
    configuration,
    status,
    address,
    creationTime,
    updateTime
)

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes(
    JsonSubTypes.Type(AlgorithmicStrategyTest::class, name = "ALGORITHMIC"),
)
abstract class StrategyTest(
    @Id @JsonSerialize(using = ToStringSerializer::class)
    val id: ObjectId = ObjectId.get(),

    val name: String,
    val description: String,
    @JsonSerialize(using = ToStringSerializer::class) val owner: ObjectId,
    val type: StrategyType,
    @JsonSerialize(using = ToStringSerializer::class) val exchange: ObjectId,
    val configuration: TradingConfiguration,

    var status: BotStatus = BotStatus.STOPPED,
    var address: BotAddress? = null,

    @Field("time.creation")
    @JsonProperty("time.creation")
    val creationTime: Instant = Instant.now(),

    @Field("time.update")
    @JsonProperty("time.update")
    val updateTime: Instant = Instant.now()
)