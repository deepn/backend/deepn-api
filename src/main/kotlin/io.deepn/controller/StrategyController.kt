package io.deepn.controller

import io.deepn.configuration.resolver.ResolveAccount
import io.deepn.entity.Account
import io.deepn.entity.response.Ok
import io.deepn.entity.strategy.Strategy
import io.deepn.repository.StrategyRepository
import io.deepn.utils.notFoundIfEmpty
import io.deepn.utils.toRemoveResponse
import org.bson.types.ObjectId
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import springfox.documentation.annotations.ApiIgnore

@RestController
@RequestMapping("/strategy/")
class StrategyController(private val repository: StrategyRepository) {

    @GetMapping
    fun getAll(@ResolveAccount @ApiIgnore account: Account): Flux<Strategy> = repository.getAllByOwner(account.id)

    @GetMapping("{id}")
    fun get(@ResolveAccount @ApiIgnore account: Account, @PathVariable id: ObjectId): Mono<Strategy> =
        repository.getByOwnerAndId(account.id, id).notFoundIfEmpty(id)


    @DeleteMapping("{id}")
    fun delete(@ResolveAccount @ApiIgnore account: Account, @PathVariable id: ObjectId): Mono<Ok> {
        //todo: check strategy deletion conditions
        return repository.removeByOwnerAndId(account.id, id).toRemoveResponse(id)
    }

}
