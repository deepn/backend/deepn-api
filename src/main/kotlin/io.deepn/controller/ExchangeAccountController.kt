package io.deepn.controller

import io.deepn.common.model.Exchange
import io.deepn.configuration.resolver.ResolveAccount
import io.deepn.entity.Account
import io.deepn.entity.ExchangeAccount
import io.deepn.entity.request.ExchangeAccountRequest
import io.deepn.entity.response.Ok
import io.deepn.repository.ExchangeAccountRepository
import io.deepn.repository.operations.ExchangeAccountOperation
import io.deepn.utils.conflictIfFilterFalse
import io.deepn.utils.notFoundIfFalse
import io.deepn.utils.toRemoveResponse
import org.bson.types.ObjectId
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import springfox.documentation.annotations.ApiIgnore
import javax.validation.Valid

@RestController
@RequestMapping("/exchange-account/")
class ExchangeAccountController(
    private val repository: ExchangeAccountRepository,
    private val operation: ExchangeAccountOperation
) {

    @GetMapping
    fun getAll(@ResolveAccount @ApiIgnore account: Account) = repository.getByOwner(account.id)

    @GetMapping("{exchange}")
    fun get(@ResolveAccount @ApiIgnore account: Account, @PathVariable exchange: Exchange) =
        repository.getByOwnerAndExchange(account.id, exchange)

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(
        @ResolveAccount @ApiIgnore account: Account,
        @RequestBody @Valid request: ExchangeAccountRequest
    ): Mono<ExchangeAccount> = operation.exists(account.id, request.name)
        .conflictIfFilterFalse()
        .flatMap { repository.insert(request.toExchangeAccount(account.id)) }

    @PutMapping("{id}")
    fun update(
        @ResolveAccount @ApiIgnore account: Account,
        @PathVariable id: ObjectId,
        @RequestBody @Valid request: ExchangeAccountRequest
    ): Mono<ExchangeAccount> = repository.existsByOwnerAndId(account.id, id)
        .notFoundIfFalse(id)
        .flatMap { operation.update(account.id, id, request.toExchangeAccount(account.id)) }

    @DeleteMapping("{id}")
    fun delete(@ResolveAccount @ApiIgnore account: Account, @PathVariable id: ObjectId): Mono<Ok> {
        return repository.removeByOwnerAndId(account.id, id).toRemoveResponse(id)
    }
}