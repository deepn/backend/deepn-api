package io.deepn.controller

import io.deepn.configuration.resolver.IPAddress
import io.deepn.configuration.security.jwt.JwtTokenProvider
import io.deepn.constant.FieldConstant.EMAIL_DELAY_MINUTES
import io.deepn.entity.LoginActivity
import io.deepn.entity.request.AccountCreationRequest
import io.deepn.entity.request.LoginRequest
import io.deepn.entity.request.ResetPasswordRequest
import io.deepn.entity.request.ResetPasswordUpdateRequest
import io.deepn.entity.response.LoginResponse
import io.deepn.entity.response.Ok
import io.deepn.entity.response.toResponse
import io.deepn.repository.AccountActivityRepository
import io.deepn.repository.AccountRepository
import io.deepn.repository.operations.AccountOperation
import io.deepn.service.EmailService
import io.deepn.utils.*
import io.deepn.validation.NoSpace
import io.deepn.validation.Username
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import springfox.documentation.annotations.ApiIgnore
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty

@RestController
@RequestMapping("/auth/")
@Validated
class AuthController(
    private val repository: AccountRepository,
    private val accountOperation: AccountOperation,
    private val tokenProvider: JwtTokenProvider,
    private val authenticationManager: ReactiveAuthenticationManager,
    private val passwordEncoder: PasswordEncoder,
    private val emailService: EmailService,
    private val accountActivityRepository: AccountActivityRepository
) {

    @PostMapping("login")
    fun login(@Valid @RequestBody credentials: LoginRequest, @IPAddress @ApiIgnore ipAddress: String): Mono<LoginResponse> =
        authenticationManager.authenticate(credentials.toAuthenticationToken())
            .zipWhen { repository.getByEmail(credentials.login) }
            .map { (authentication, account) -> LoginResponse(account, tokenProvider.createToken(authentication)) }
            .zipWhen { accountActivityRepository.insert(LoginActivity(ipAddress, it.account.id)) }
            .map { (response, _) -> response }

    @PostMapping("register")
    @ResponseStatus(HttpStatus.CREATED)
    fun register(@Valid @RequestBody accountRequest: AccountCreationRequest): Mono<LoginResponse> =
        repository.insert(accountRequest.encode(passwordEncoder).toAccount())
            .map { LoginResponse(it, tokenProvider.createToken(it.toAuthenticationToken())) }

    @GetMapping("available/email")
    fun isEmailAvailable(@RequestParam("email") @Email(message = "email") @NotEmpty email: String): Mono<Ok> =
        accountOperation.isEmailAvailable(email.toLowerCase().normalizeSpaces()).map { it.toResponse() }

    @GetMapping("available/username")
    fun isUsernameAvailable(@RequestParam("username") @NoSpace(message = "username") @NotEmpty @Username username: String): Mono<Ok> =
        accountOperation.isUsernameAvailable(username.toLowerCase().normalizeSpaces()).map { it.toResponse() }


    @PutMapping("update-password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updateResetPassword(@Valid @RequestBody request: ResetPasswordUpdateRequest): Mono<Ok> {
        return repository.getByEmailAndResetToken(request.email, request.token)
            .unauthorizedIfEmpty()
            .flatMap { accountOperation.updatePassword(it.id, passwordEncoder.encode(request.newPassword)) }
            .map { Ok(it.modifiedCount) }
    }

    @PostMapping("reset-password")
    fun reset(@Valid @RequestBody request: ResetPasswordRequest): Mono<Ok> =
        repository.getByEmail(request.email)
            .notFoundIfEmpty(request.email)
            .filter { it.resetTokenSendTime.minutesFromNow() > EMAIL_DELAY_MINUTES }
            .constraintValidationIfEmpty("duration")
            .flatMap { accountOperation.resetPassword(it, tokenProvider.createResetPasswordToken(it)) }
            .map { Ok(emailService.resetPassword(it)) }

}
