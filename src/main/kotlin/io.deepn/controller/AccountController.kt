package io.deepn.controller

import io.deepn.configuration.resolver.ResolveAccount
import io.deepn.configuration.security.filter.UnconfirmedAccount
import io.deepn.configuration.security.jwt.JwtTokenProvider
import io.deepn.constant.FieldConstant.EMAIL_DELAY_MINUTES
import io.deepn.entity.Account
import io.deepn.entity.request.ConfirmationCode
import io.deepn.entity.request.PasswordUpdateRequest
import io.deepn.entity.response.NoContent
import io.deepn.entity.response.Ok
import io.deepn.entity.response.TokenResponse
import io.deepn.repository.AccountRepository
import io.deepn.repository.operations.AccountOperation
import io.deepn.service.EmailService
import io.deepn.utils.constraintValidationIfEmpty
import io.deepn.utils.minutesFromNow
import io.deepn.utils.toAuthenticationToken
import io.deepn.utils.unauthorizedIfEmpty
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import springfox.documentation.annotations.ApiIgnore
import javax.validation.Valid

@RestController
@RequestMapping("/account/")
class AccountController(
    private val repository: AccountRepository,
    private val accountOperation: AccountOperation,
    private val emailService: EmailService,
    private val tokenProvider: JwtTokenProvider,
    private val passwordEncoder: PasswordEncoder
) {

    @UnconfirmedAccount
    @GetMapping
    fun get(@ResolveAccount @ApiIgnore account: Account) = account.toMono()

    @UnconfirmedAccount
    @PutMapping("update-password")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updatePassword(
        @ResolveAccount @ApiIgnore account: Account,
        @RequestBody @Valid request: PasswordUpdateRequest
    ): Mono<NoContent> {
        return repository.getByEmail(account.email)
            .filter { passwordEncoder.matches(request.currentPassword, it.password) }
            .unauthorizedIfEmpty()
            .flatMap { accountOperation.updatePassword(it.id, passwordEncoder.encode(request.newPassword)) }
            .map { NoContent }
    }

    @UnconfirmedAccount
    @PatchMapping("confirm")
    fun confirm(
        @ResolveAccount @ApiIgnore account: Account,
        @RequestBody @Valid message: ConfirmationCode
    ): Mono<Ok> = accountOperation.confirmCode(account.id, message.code)
        .map { Ok(it.modifiedCount) }


    @UnconfirmedAccount
    @PostMapping("confirm")
    fun sendConfirmation(@ResolveAccount @ApiIgnore account: Account): Mono<Ok> =
        account.toMono()
            .filter { it.confirmationSendTime.minutesFromNow() > EMAIL_DELAY_MINUTES }
            .constraintValidationIfEmpty("duration")
            .flatMap { accountOperation.generateConfirmationCode(account.id) }
            .map { Ok(emailService.confirmationCode(it)) }


    @GetMapping("newToken")
    fun generateNewToken(@ResolveAccount @ApiIgnore account: Account) = TokenResponse(
        tokenProvider.createToken(account.toAuthenticationToken())
    ).toMono()


    /**
     * TODO: Remove all element linked to account like bots, strategies etc...
     */
    @UnconfirmedAccount
    @DeleteMapping
    fun delete(@ResolveAccount @ApiIgnore account: Account): Mono<Void> = repository.delete(account)
}
