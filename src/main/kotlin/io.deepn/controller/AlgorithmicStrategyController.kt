package io.deepn.controller

import io.deepn.configuration.resolver.ResolveAccount
import io.deepn.entity.Account
import io.deepn.entity.request.AlgorithmicStrategyRequest
import io.deepn.entity.strategy.impl.AlgorithmicStrategy
import io.deepn.repository.ExchangeAccountRepository
import io.deepn.repository.StrategyRepository
import io.deepn.repository.operations.StrategyOperation
import io.deepn.utils.conflictIfFilterFalse
import io.deepn.utils.conflictIfFilterTrue
import io.deepn.utils.notFoundIfFalse
import org.bson.types.ObjectId
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import springfox.documentation.annotations.ApiIgnore
import javax.validation.Valid

@RestController
@RequestMapping("/strategy/algorithmic/")
class AlgorithmicStrategyController(
    private val operation: StrategyOperation,
    private val repository: StrategyRepository,
    private val exchangeAccountRepository: ExchangeAccountRepository
) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(
        @ResolveAccount @ApiIgnore account: Account,
        @RequestBody @Valid request: AlgorithmicStrategyRequest
    ): Mono<AlgorithmicStrategy> = operation.exists(account.id, request.name)
        .conflictIfFilterFalse()
        .flatMap { exchangeAccountRepository.existsByOwnerAndId(account.id, request.exchange) }
        .notFoundIfFalse("exchange" to request.exchange)
        .flatMap { repository.insert(request.toStrategy(account.id)) }


    @PutMapping("{id}")
    fun update(
        @ResolveAccount @ApiIgnore account: Account,
        @PathVariable id: ObjectId,
        @RequestBody @Valid request: AlgorithmicStrategyRequest
    ): Mono<AlgorithmicStrategy> = operation.exists(account.id, request.name)
        .conflictIfFilterTrue()
        .flatMap { exchangeAccountRepository.existsByOwnerAndId(account.id, request.exchange) }
        .notFoundIfFalse("exchange" to request.exchange)
        .flatMap { repository.existsByOwnerAndId(account.id, id) }
        .notFoundIfFalse(id)
        .flatMap { operation.updateAlgorithmic(account.id, id, request) }

}
