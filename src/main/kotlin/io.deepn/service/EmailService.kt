package io.deepn.service

import com.sendgrid.*
import io.deepn.entity.BasicAccountInformation
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringWebFluxTemplateEngine
import java.util.*


data class EmailTemplate(
    val recipients: List<String>,
    val subject: String,
    val template: String,
    val properties: Map<String, Any> = emptyMap()
)

data class EmailResponse(val sent: Boolean) {
    companion object {
        fun sent() = EmailResponse(true)
        fun failed() = EmailResponse(false)
    }
}

@Component
class EmailService(private val templateEngine: SpringWebFluxTemplateEngine) {

    private val logger = KotlinLogging.logger {}

    @Value("\${spring.mail.username}")
    private lateinit var accountEmail: String

    @Value("\${email.sendgrid.api}")
    private lateinit var key: String

    fun confirmationCode(account: BasicAccountInformation): Boolean {
        val confirmationCode = account.confirmationCode
        if (confirmationCode.isNullOrBlank()) return false
        return sendHtml(
            EmailTemplate(
                listOf(account.email),
                "[Deepn] Confirm your Account",
                "account-validation",
                mapOf("code" to confirmationCode, "username" to account.username)
            )
        )
    }

    fun resetPassword(account: BasicAccountInformation): Boolean {
        sendHtml(
            EmailTemplate(
                listOf(account.email),
                "[Deepn] Reset password",
                "reset-password",
                mapOf("username" to account.username, "token" to (account.resetToken ?: ""))
            )
        )
        return true
    }

    fun sendHtml(template: EmailTemplate): Boolean {
        val sendGrid = SendGrid(key)
        val content =
            Content(
                "text/html", templateEngine.process(
                    template.template,
                    Context(Locale.ENGLISH, template.properties)
                )
            )

        val mail = Mail()
        val personalization = Personalization()
        mail.from = Email(accountEmail)
        mail.subject = template.subject
        mail.addContent(content)
        template.recipients.forEach { personalization.addTo(Email(it)) }
        mail.addPersonalization(personalization)

        return kotlin.runCatching {
            val request = Request().apply {
                method = Method.POST
                endpoint = "mail/send"
                body = mail.build()
            }

            return@runCatching sendGrid.api(request).statusCode < 300
        }.getOrElse {
            it.printStackTrace()
            false
        }

    }
}

