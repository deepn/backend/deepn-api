package io.deepn.utils

import io.deepn.entity.Account
import io.deepn.entity.request.LoginRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalUnit
import java.util.*

typealias AuthenticationToken = UsernamePasswordAuthenticationToken
typealias TemporalAmount = Pair<Number, TemporalUnit>

fun Date.plus(temporalAmount: TemporalAmount): Date {
    return Date.from(this.toInstant().plus(temporalAmount.first.toLong(), temporalAmount.second))
}

fun Date.minus(temporalAmount: TemporalAmount): Date {
    return Date.from(this.toInstant().minus(temporalAmount.first.toLong(), temporalAmount.second))
}

fun Number.ms() = TemporalAmount(this, ChronoUnit.MILLIS)

fun Number.min() = TemporalAmount(this, ChronoUnit.MINUTES)

fun Number.sec() = TemporalAmount(this, ChronoUnit.SECONDS)

fun Number.hours() = TemporalAmount(this, ChronoUnit.HOURS)

fun Number.day() = TemporalAmount(this, ChronoUnit.DAYS)

fun Number.month() = TemporalAmount(this, ChronoUnit.MONTHS)

fun Instant?.minutesFromNow(): Long = if(this == null) Long.MAX_VALUE else
    Duration.between(this, Instant.now()).toMinutes()

fun LoginRequest.toAuthenticationToken() = AuthenticationToken(login, password)

fun Account.toAuthenticationToken() = AuthenticationToken(id.toString(), password)

fun Account.toUser(): UserDetails =
    User.withUsername(id.toHexString()).password(password).authorities(role.name).build()

fun generateRandomCode(): String = IntRange(0, 3).joinToString("",
    transform = { Random().nextInt(10).toString() })

fun String.normalizeSpaces() = this.trim().replace(Regex("\\s+"), " ")

fun String.toFindRegex() = "^${Regex.escape(this)}\$".toRegex(setOf(RegexOption.LITERAL, RegexOption.IGNORE_CASE))