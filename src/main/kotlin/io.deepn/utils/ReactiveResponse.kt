package io.deepn.utils

import io.deepn.common.exception.Conflict
import io.deepn.common.exception.ConstraintViolation
import io.deepn.common.exception.NotFound
import io.deepn.common.exception.Unauthorized
import io.deepn.entity.response.Ok
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty

fun Mono<Long>.toRemoveResponse(resource: Any) = this
    .filter { removed -> removed > 0 }
    .map { Ok() }
    .switchIfEmpty { Mono.error(NotFound(resource)) }

fun Mono<Boolean>.filterTrue(): Mono<Boolean> = this.filter { it }

fun Mono<Boolean>.filterFalse(): Mono<Boolean> = this.filter { !it }

fun Mono<Boolean>.conflictIfFilterFalse() = this.filterFalse().switchIfEmpty { Mono.error(Conflict) }

fun Mono<Boolean>.conflictIfFilterTrue() = this.filterTrue().switchIfEmpty { Mono.error(Conflict) }

fun <T> Mono<T>.conflictIfEmpty() = this.switchIfEmpty { Mono.error(Conflict) }

fun <T> Mono<T>.unauthorizedIfEmpty() = this.switchIfEmpty { Mono.error(Unauthorized) }

fun Mono<Boolean>.notFoundIfTrue(resource: Any) =
    this.filterFalse().switchIfEmpty { Mono.error(NotFound(resource)) }

fun Mono<Boolean>.notFoundIfFalse(resource: Any) =
    this.filterTrue().switchIfEmpty { Mono.error(NotFound(resource)) }

fun <T> Mono<T>.notFoundIfEmpty(resource: Any) = this.switchIfEmpty { Mono.error(NotFound(resource)) }


fun Mono<Boolean>.constraintValidationIfTrue(vararg fields: String) =
    this.filterFalse().switchIfEmpty {
        Mono.error(ConstraintViolation(setOf(*fields)))
    }

fun Mono<Boolean>.constraintValidationIfFalse(vararg fields: String) =
    this.filterTrue().switchIfEmpty {
        Mono.error(ConstraintViolation(setOf(*fields)))
    }

fun <T> Mono<T>.constraintValidationIfEmpty(vararg fields: String) = this.switchIfEmpty {
    Mono.error(ConstraintViolation(setOf(*fields)))
}