package io.deepn.utils

import org.springframework.data.mongodb.core.mapping.Field
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import kotlin.reflect.KProperty
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.jvm.javaField

fun KProperty<*>.fieldName() = this.javaField?.getAnnotation(Field::class.java)?.value
    ?: this.findAnnotation<Field>()?.value ?: this.name

fun updates(vararg values: Pair<KProperty<*>, Any?>): Update {
    val update = Update()
    values.forEach { (property, value) ->
        update.set(property.fieldName(), value)
    }
    return update
}

fun Query.includes(vararg properties: KProperty<*>) {
    properties.map { it.fieldName() }.toTypedArray().forEach { this.fields().include(it) }
}

fun Criteria.toQuery() = Query(this)