package io.deepn

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication(exclude = [WebMvcAutoConfiguration::class])
@ConfigurationPropertiesScan
class DeepnApplication

fun main(args: Array<String>) {
    runApplication<DeepnApplication>(*args)
}
