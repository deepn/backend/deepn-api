package io.deepn.exception

import io.deepn.common.exception.*
import org.springframework.dao.DuplicateKeyException
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.bind.support.WebExchangeBindException
import javax.validation.ConstraintViolationException


@RestControllerAdvice
class GlobalExceptionController {

    @ExceptionHandler(value = [BadCredentialsException::class])
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    fun badCredentialsException(ex: Exception): Nothing? = null

    @ExceptionHandler(WebExchangeBindException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun handleWebExchangeBindException(e: WebExchangeBindException) =
        ConstraintViolation(e.fieldErrors.map { it.field }.toSet())

    @ExceptionHandler(DuplicateKeyException::class)
    @ResponseStatus(HttpStatus.CONFLICT)
    fun duplicate(e: DuplicateKeyException) = Conflict

    @ExceptionHandler(ConstraintViolationException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun constraintViolationException(e: ConstraintViolationException) =
        ConstraintViolation(e.constraintViolations.map { it.message }.toSet())

    @ExceptionHandler(APIError::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun apiError(e: APIError) = e

    @ExceptionHandler(Conflict::class)
    @ResponseStatus(HttpStatus.CONFLICT)
    fun conflict(e: APIError) = e

    @ExceptionHandler(NotFound::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun notFound(e: NotFound) = e as APIError

    @ExceptionHandler(Unauthorized::class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    fun unauthorized() = Unauthorized

    @ExceptionHandler(InternalServer::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun internalServer() = InternalServer

}