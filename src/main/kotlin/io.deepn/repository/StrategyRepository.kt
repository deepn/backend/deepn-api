package io.deepn.repository

import io.deepn.entity.strategy.Strategy
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Repository
interface StrategyRepository : ReactiveMongoRepository<Strategy, String> {

    fun existsByOwnerAndId(owner: ObjectId, id: ObjectId): Mono<Boolean>

    fun getByOwnerAndId(owner: ObjectId, id: ObjectId): Mono<Strategy>

    fun getAllByOwner(owner: ObjectId): Flux<Strategy>

    fun removeByOwnerAndId(owner: ObjectId, id: ObjectId): Mono<Long>

}

