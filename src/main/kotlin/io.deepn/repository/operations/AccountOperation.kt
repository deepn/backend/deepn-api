package io.deepn.repository.operations

import com.mongodb.client.result.UpdateResult
import io.deepn.entity.Account
import io.deepn.entity.BasicAccountInformation
import io.deepn.utils.*
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.FindAndModifyOptions.options
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.exists
import org.springframework.data.mongodb.core.findAndModify
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.data.mongodb.core.query.ne
import org.springframework.data.mongodb.core.query.regex
import org.springframework.data.mongodb.core.updateFirst
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.time.Instant


@Service
class AccountOperation(private val template: ReactiveMongoTemplate) {

    fun isEmailAvailable(email: String): Mono<Boolean> {
        return template.exists<Account>(
            Account::email.regex(email.toFindRegex()).toQuery()
        ).map { it.not() }
    }

    fun isUsernameAvailable(username: String): Mono<Boolean> {
        return template.exists<Account>(
            Account::username.regex(username.toFindRegex()).toQuery(),
        ).map { it.not() }
    }

    fun confirmCode(id: ObjectId, code: String): Mono<UpdateResult> {
        return template.updateFirst<Account>(
            Criteria().andOperator(
                Account::id isEqualTo id,
                Account::confirmationCode isEqualTo code,
                (Account::confirmed ne true)
            ).toQuery(), updates(Account::confirmed to true)
        )
    }

    fun generateConfirmationCode(id: ObjectId): Mono<BasicAccountInformation> {
        return template.findAndModify(
            (Account::id isEqualTo id).toQuery().apply {
                includes(Account::username, Account::email, Account::confirmationCode)
            }, updates(
                Account::confirmationCode to generateRandomCode(),
                Account::confirmationSendTime to Instant.now()
            ), options().returnNew(true)
        )
    }

    fun updatePassword(id: ObjectId, newPassword: String): Mono<UpdateResult> {
        return template.updateFirst<Account>(
            Criteria().andOperator(
                Account::id isEqualTo id
            ).toQuery(),
            updates(
                Account::password to newPassword,
                Account::updateTime to Instant.now(),
                Account::resetToken to null
            )
        )
    }

    fun resetPassword(account: Account, token: String): Mono<BasicAccountInformation> =
        template.findAndModify(
            (Account::id isEqualTo account.id).toQuery(),
            updates(
                Account::resetToken to token,
                Account::resetTokenSendTime to Instant.now(),
            ),
            options().returnNew(true)
        )

}