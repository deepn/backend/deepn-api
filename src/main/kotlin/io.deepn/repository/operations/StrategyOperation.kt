package io.deepn.repository.operations

import io.deepn.entity.request.AlgorithmicStrategyRequest
import io.deepn.entity.strategy.Strategy
import io.deepn.entity.strategy.impl.AlgorithmicStrategy
import io.deepn.utils.toFindRegex
import io.deepn.utils.toQuery
import io.deepn.utils.updates
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.FindAndModifyOptions
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.exists
import org.springframework.data.mongodb.core.findAndModify
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.data.mongodb.core.query.regex
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.time.Instant


@Service
class StrategyOperation(private val template: ReactiveMongoTemplate) {

    fun exists(owner: ObjectId, name: String): Mono<Boolean> {
        return template.exists<Strategy>(
            Criteria().andOperator(
                Strategy::owner isEqualTo owner,
                Strategy::name.regex(name.toFindRegex())
            ).toQuery()
        )
    }

    fun updateAlgorithmic(
        owner: ObjectId,
        id: ObjectId,
        strategyUpdate: AlgorithmicStrategyRequest
    ): Mono<AlgorithmicStrategy> {
        return template.findAndModify(
            Criteria().andOperator(
                AlgorithmicStrategy::owner isEqualTo owner,
                AlgorithmicStrategy::id isEqualTo id
            ).toQuery(),
            updates(
                AlgorithmicStrategy::name to strategyUpdate.name,
                AlgorithmicStrategy::description to strategyUpdate.description,
                AlgorithmicStrategy::source to strategyUpdate.source,
                AlgorithmicStrategy::updateTime to Instant.now()
            ), FindAndModifyOptions.options().returnNew(true)
        )
    }

}

