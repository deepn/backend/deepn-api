package io.deepn.repository.operations

import io.deepn.entity.ExchangeAccount
import io.deepn.utils.toFindRegex
import io.deepn.utils.toQuery
import io.deepn.utils.updates
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.FindAndModifyOptions.options
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.findAndModify
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.data.mongodb.core.query.regex
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class ExchangeAccountOperation(private val template: ReactiveMongoTemplate) {

    fun exists(owner: ObjectId, name: String): Mono<Boolean> {
        return template.exists(
            Criteria().andOperator(
                ExchangeAccount::owner isEqualTo owner,
                ExchangeAccount::name.regex(name.toFindRegex())
            ).toQuery(), ExchangeAccount::class.java
        )
    }

    fun update(owner: ObjectId, id: ObjectId, account: ExchangeAccount): Mono<ExchangeAccount> {
        return template.findAndModify(
            Criteria().andOperator(
                ExchangeAccount::owner isEqualTo owner,
                ExchangeAccount::id isEqualTo id
            ).toQuery(), updates(
                ExchangeAccount::name to account.name,
                ExchangeAccount::exchange to account.exchange,
                ExchangeAccount::credentials to account.credentials
            ), options().returnNew(true)
        )
    }

}

