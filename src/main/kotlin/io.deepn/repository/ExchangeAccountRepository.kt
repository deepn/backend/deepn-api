package io.deepn.repository

import io.deepn.common.model.Exchange
import io.deepn.entity.ExchangeAccount
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface ExchangeAccountRepository : ReactiveMongoRepository<ExchangeAccount, String> {

    fun existsByOwnerAndId(owner: ObjectId, id: ObjectId): Mono<Boolean>

    fun getByOwner(owner: ObjectId): Flux<ExchangeAccount>

    fun getByOwnerAndExchange(owner: ObjectId, exchange: Exchange): Flux<ExchangeAccount>

    fun removeByOwnerAndId(owner: ObjectId, id: ObjectId): Mono<Long>

}