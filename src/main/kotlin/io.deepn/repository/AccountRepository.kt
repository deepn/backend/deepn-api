package io.deepn.repository

import io.deepn.entity.Account
import io.deepn.entity.AccountActivity
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono

@Repository
interface AccountRepository : ReactiveMongoRepository<Account, String> {

    fun getByEmail(email: String): Mono<Account>

    fun getByEmailAndResetToken(email: String, token: String): Mono<Account>

}

@Repository
interface AccountActivityRepository : ReactiveMongoRepository<AccountActivity, String>