package io.deepn.validation

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import io.deepn.utils.normalizeSpaces
import java.io.IOException
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import javax.validation.Payload
import kotlin.reflect.KClass


@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Repeatable
@MustBeDocumented
@Constraint(validatedBy = [NoSpaceValidation::class])
annotation class NoSpace(
    val message: String = "must not contain space",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)

@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Repeatable
@MustBeDocumented
@Constraint(validatedBy = [UsernameValidation::class])
annotation class Username(
    val message: String = "only characters and numbers",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)

@Target(AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
@Repeatable
@MustBeDocumented
@Constraint(validatedBy = [NullOrNotBlankValidation::class])
annotation class NullOrNotBlank(
    val message: String = "must be null or not blank",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Payload>> = []
)

class NoSpaceValidation : ConstraintValidator<NoSpace, String> {
    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        if (value == null) return false
        return value.chars().noneMatch(Character::isWhitespace)
    }
}

class ToLowerCase : StdDeserializer<String?>(String::class.java) {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): String {
        return _parseString(p, ctxt).toLowerCase()
    }
}

class NormalizeSpace : StdDeserializer<String?>(String::class.java) {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): String {
        return _parseString(p, ctxt).normalizeSpaces()
    }
}

class UsernameValidation : ConstraintValidator<Username, String> {
    override fun isValid(value: String, context: ConstraintValidatorContext?): Boolean {
        return value.all { it.isLetterOrDigit() }
    }
}

class NullOrNotBlankValidation : ConstraintValidator<NullOrNotBlank, String> {
    override fun isValid(value: String?, context: ConstraintValidatorContext?): Boolean {
        return value == null || value.isNotBlank()
    }
}