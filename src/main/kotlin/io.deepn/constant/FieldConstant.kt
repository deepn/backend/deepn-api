package io.deepn.constant

object FieldConstant {

    const val MIN_SIZE_NAME = 4
    const val MAX_SIZE_NAME = 25

    const val MIN_SIZE_PASSWORD = 8
    const val MAX_SIZE_PASSWORD = 60

    const val MAX_SIZE_DESCRIPTION = 200

    const val MAX_SIZE_SOURCE = 4000

    const val CODE_SIZE = 4

    const val EMAIL_DELAY_MINUTES = 5

}