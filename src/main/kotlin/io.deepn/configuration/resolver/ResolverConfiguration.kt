package io.deepn.configuration.resolver

import io.deepn.entity.Account
import org.springframework.context.annotation.Configuration
import org.springframework.core.MethodParameter
import org.springframework.web.reactive.BindingContext
import org.springframework.web.reactive.config.WebFluxConfigurer
import org.springframework.web.reactive.result.method.HandlerMethodArgumentResolver
import org.springframework.web.reactive.result.method.annotation.ArgumentResolverConfigurer
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import kotlin.reflect.KClass


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class ResolveAccount

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class IPAddress


fun attributeResolver(
    annotation: KClass<out Annotation>,
    key: String
) = object : HandlerMethodArgumentResolver {

    override fun supportsParameter(parameter: MethodParameter) = parameter.hasParameterAnnotation(annotation.java)

    override fun resolveArgument(
        parameter: MethodParameter,
        bindingContext: BindingContext,
        exchange: ServerWebExchange
    ): Mono<Any> = exchange.getRequiredAttribute<Any>(key).toMono()

}

@Configuration
class ResolverConfiguration : WebFluxConfigurer {

    override fun configureArgumentResolvers(configurer: ArgumentResolverConfigurer) {
        configurer.addCustomResolver(attributeResolver(ResolveAccount::class, Account::class.java.name))
        configurer.addCustomResolver(attributeResolver(IPAddress::class, "ip"))
    }

}