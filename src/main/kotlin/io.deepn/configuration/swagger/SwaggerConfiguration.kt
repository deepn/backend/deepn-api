package io.deepn.configuration.swagger

import org.bson.types.ObjectId
import org.reactivestreams.Publisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.ResponseEntity
import org.springframework.web.reactive.config.WebFluxConfigurer
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket


@Configuration
class SwaggerConfig : WebFluxConfigurer {
    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.OAS_30)
            .genericModelSubstitutes(
                Mono::class.java, Flux::class.java,
                Publisher::class.java, ResponseEntity::class.java
            )
            .directModelSubstitute(ObjectId::class.java, String::class.java)
            .pathMapping("/api")
            .select()
            .apis(RequestHandlerSelectors.basePackage("io.deepn"))
            .paths(PathSelectors.any())
            .build()
    }

}