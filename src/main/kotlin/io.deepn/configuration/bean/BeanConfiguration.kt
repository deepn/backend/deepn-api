package io.deepn.configuration.bean

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.*
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.deepn.common.exception.APIError
import io.deepn.common.exception.ErrorField
import org.bson.types.ObjectId
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.reactive.config.WebFluxConfigurer
import java.time.Instant
import kotlin.reflect.full.findAnnotation

@Configuration
class BeanConfiguration {

    @Bean
    fun objectMapper(): ObjectMapper {
        val mapper = Jackson2ObjectMapperBuilder()
            .serializationInclusion(JsonInclude.Include.NON_EMPTY)
            .serializerByType(ObjectId::class.java, ObjectIdSerializer())
            .serializerByType(Instant::class.java, InstantSerializer())
            .serializerByType(APIError::class.java, APIErrorSerializer())
            .serializerByType(Pair::class.java, PairSerializer())
            .createXmlMapper(false).build<ObjectMapper>()
            .configure(MapperFeature.ALLOW_COERCION_OF_SCALARS, false)
            .configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true)

        mapper.registerModule(KotlinModule())
        return mapper
    }

    @Bean
    fun jackson2JsonEncoder(mapper: ObjectMapper): Jackson2JsonEncoder {
        return Jackson2JsonEncoder(mapper)
    }

    @Bean
    fun jackson2JsonDecoder(mapper: ObjectMapper): Jackson2JsonDecoder {
        return Jackson2JsonDecoder(mapper)
    }

    @Bean
    fun webFluxConfigurer(encoder: Jackson2JsonEncoder, decoder: Jackson2JsonDecoder): WebFluxConfigurer {
        return object : WebFluxConfigurer {
            override fun configureHttpMessageCodecs(configurer: ServerCodecConfigurer) {
                configurer.defaultCodecs().jackson2JsonEncoder(encoder)
                configurer.defaultCodecs().jackson2JsonDecoder(decoder)
            }
        }
    }

}

class ObjectIdSerializer : JsonSerializer<ObjectId>() {
    override fun serialize(value: ObjectId?, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeString(value?.toHexString())
    }
}

class InstantSerializer : JsonSerializer<Instant>() {
    override fun serialize(value: Instant, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeNumber(value.toEpochMilli())
    }
}

class APIErrorSerializer : JsonSerializer<APIError>() {
    override fun serialize(value: APIError, gen: JsonGenerator?, serializers: SerializerProvider?) {
        val fieldName = value::class.findAnnotation<ErrorField>()?.value ?: "value"

        gen?.writeStartObject()
        gen?.writeFieldName("error")
        gen?.writeStartObject()
        gen?.writeNumberField("code", value.errorCode.code)
        gen?.writeStringField("name", value.errorCode.name)
        value.value?.let {
            gen?.writeFieldName(fieldName)
            gen?.writeObject(value.value)
        }
    }
}

class PairSerializer : JsonSerializer<Pair<*,*>>() {
    override fun serialize(value: Pair<*,*>, gen: JsonGenerator?, serializers: SerializerProvider?) {
        gen?.writeStartObject()
        gen?.writeObjectField("key", value.first)
        gen?.writeObjectField("value", value.second)
    }
}