package io.deepn.configuration.email

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.thymeleaf.spring5.SpringWebFluxTemplateEngine
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver
import org.thymeleaf.templatemode.TemplateMode
import java.nio.charset.StandardCharsets

@Configuration
class EmailTemplateConfiguration {

    @Bean
    fun springTemplateEngine(): SpringWebFluxTemplateEngine {
        val templateEngine = SpringWebFluxTemplateEngine()
        templateEngine.addTemplateResolver(htmlTemplateResolver())
        return templateEngine
    }

    @Bean
    fun htmlTemplateResolver(): SpringResourceTemplateResolver = SpringResourceTemplateResolver().apply {
        prefix = "classpath:/templates/emails/"
        suffix = ".html"
        templateMode = TemplateMode.HTML
        characterEncoding = StandardCharsets.UTF_8.name()
    }
}