package io.deepn.configuration.security


import io.deepn.configuration.security.filter.ConfirmedAccountFilter
import io.deepn.configuration.security.jwt.JwtTokenAuthenticationFilter
import io.deepn.configuration.security.jwt.JwtTokenProvider
import io.deepn.entity.Account
import io.deepn.repository.AccountRepository
import io.deepn.utils.toUser
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UserDetailsRepositoryReactiveAuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.SecurityWebFiltersOrder
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.context.NoOpServerSecurityContextRepository
import org.springframework.security.web.server.savedrequest.NoOpServerRequestCache
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping


@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class SecurityConfiguration(val repository: AccountRepository) {

    @Bean
    fun springWebFilterChain(
        http: ServerHttpSecurity,
        authenticationManager: ReactiveAuthenticationManager,
        tokenProvider: JwtTokenProvider,
        requestMappingHandlerMapping: RequestMappingHandlerMapping
    ): SecurityWebFilterChain {
        return http
            .csrf().disable()
            .formLogin().disable()
            .logout().disable()
            .httpBasic().disable()
            .requestCache().requestCache(NoOpServerRequestCache.getInstance())
            .and().securityContextRepository(NoOpServerSecurityContextRepository.getInstance())
            .authenticationManager(authenticationManager)
            .authorizeExchange {
                it.pathMatchers(
                    "/v2/api-docs",
                    "/swagger-resources/**",
                    "/v3/api-docs",
                    "/swagger-ui.html",
                    "/swagger-ui/**"
                ).permitAll()
                it.pathMatchers("/auth/**").permitAll()
                it.anyExchange().authenticated()
            }
            .addFilterAt(JwtTokenAuthenticationFilter(tokenProvider), SecurityWebFiltersOrder.HTTP_BASIC)
            .addFilterAfter(ConfirmedAccountFilter(requestMappingHandlerMapping), SecurityWebFiltersOrder.AUTHENTICATION)
            .build()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun userDetailsService(): ReactiveUserDetailsService {
        return ReactiveUserDetailsService { id ->
            repository.getByEmail(id.toLowerCase()).map(Account::toUser)
        }
    }

    @Bean
    fun reactiveAuthenticationManager(
        passwordEncoder: PasswordEncoder,
        userDetailsService: ReactiveUserDetailsService
    ): ReactiveAuthenticationManager {
        return UserDetailsRepositoryReactiveAuthenticationManager(userDetailsService).apply {
            setPasswordEncoder(passwordEncoder)
        }
    }


}
