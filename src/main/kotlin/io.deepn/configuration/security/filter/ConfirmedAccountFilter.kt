package io.deepn.configuration.security.filter

import io.deepn.entity.Account
import org.springframework.http.HttpStatus
import org.springframework.web.method.HandlerMethod
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class UnconfirmedAccount

class ConfirmedAccountFilter(
    private val requestMappingHandlerMapping: RequestMappingHandlerMapping
) : WebFilter {


    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        return requestMappingHandlerMapping.getHandler(exchange)
            .defaultIfEmpty(Any())
            .flatMap {
                if (it is HandlerMethod && it.hasMethodAnnotation(UnconfirmedAccount::class.java).not()) {
                    exchange.getAttribute<Account>(Account::class.java.name)?.let { account ->
                        if (account.confirmed.not()) {
                            exchange.response.statusCode = HttpStatus.UNAUTHORIZED
                            return@flatMap Mono.empty()
                        }
                    }
                    chain.filter(exchange)
                } else chain.filter(exchange)
            }
    }


}