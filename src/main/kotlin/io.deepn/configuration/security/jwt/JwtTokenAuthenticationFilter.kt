package io.deepn.configuration.security.jwt

import io.deepn.entity.Account
import org.springframework.http.HttpHeaders
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.security.core.context.ReactiveSecurityContextHolder.withAuthentication
import org.springframework.util.StringUtils
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

const val HEADER_PREFIX = "Bearer "

fun resolveToken(request: ServerHttpRequest): String {
    val bearerToken = request.headers.getFirst(HttpHeaders.AUTHORIZATION).orEmpty()
    return if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(HEADER_PREFIX)) {
        bearerToken.substring(HEADER_PREFIX.length)
    } else ""
}

class JwtTokenAuthenticationFilter(private val tokenProvider: JwtTokenProvider) : WebFilter {


    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        return tokenProvider.validateToken(resolveToken(exchange.request)).flatMap {
            exchange.attributes["authenticated"] = true
            //TODO : check if IP is valid
            exchange.request.remoteAddress?.let { address -> exchange.attributes["ip"] = address.hostString }
            
            if (it.authenticationToken.isAuthenticated) {
                exchange.attributes[Account::class.java.name] = it.account

                chain.filter(exchange).contextWrite(withAuthentication(it.authenticationToken))
            } else
                chain.filter(exchange)
        }
    }


}