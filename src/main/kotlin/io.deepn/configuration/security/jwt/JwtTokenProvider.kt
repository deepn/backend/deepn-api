package io.deepn.configuration.security.jwt

import io.deepn.entity.Account
import io.deepn.repository.AccountRepository
import io.deepn.utils.*
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.util.*
import javax.crypto.SecretKey


data class TokenValidationResponse(val account: Account?, val authenticationToken: AuthenticationToken)

fun AuthenticationToken.toResponse(account: Account? = null) = TokenValidationResponse(account, this)

@Component
class JwtTokenProvider(val accountRepository: AccountRepository) {

    @Value("\${jwt.secretKey:default-secret-156d}")
    private lateinit var secret: String

    @Value("\${jwt.validityTime:120}")
    private var validityTime: Long = 120

    private val secretKey: SecretKey by lazy {
        Keys.hmacShaKeyFor(secret.toByteArray())
    }

    fun createToken(authentication: Authentication, neverExpire: Boolean = false): String {
        val currentDate = Date()
        return Jwts.builder()
            .setClaims(Jwts.claims().apply {
                this["role"] = authentication.authorities.map { it.authority }.firstOrNull().orEmpty()
            })
            .setSubject(authentication.name)
            .setIssuedAt(currentDate)
            .setExpiration(
                if (neverExpire) currentDate.plus(validityTime.min())
                else currentDate.plus(15.day())
            )
            .signWith(secretKey)
            .compact()
    }

    fun createResetPasswordToken(account: Account): String {
        val currentDate = Date()
        return Jwts.builder()
            .setClaims(Jwts.claims().apply {
                this["role"] = account.role
            })
            .setSubject(account.email)
            .setIssuedAt(currentDate)
            .setExpiration(currentDate.plus(2.hours()))
            .signWith(Keys.hmacShaKeyFor(account.password.toByteArray()))
            .compact()
    }

    private fun String.getClaims() = Jwts.parserBuilder().setSigningKey(secretKey)
        .build().parseClaimsJws(this).body

    fun validateToken(token: String): Mono<TokenValidationResponse> {
        val invalidAuthentication = AuthenticationToken(null, null)
        return kotlin.runCatching {
            val claims = token.getClaims()
            val authoritiesClaim = claims["role"]

            val authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(authoritiesClaim.toString())

            val principal = User(claims.subject, "", authorities)

            accountRepository.findById(claims.subject)
                .map { AuthenticationToken(principal, null, authorities).toResponse(it) }
                .defaultIfEmpty(invalidAuthentication.toResponse())
        }.getOrDefault(invalidAuthentication.toResponse().toMono())
    }

}