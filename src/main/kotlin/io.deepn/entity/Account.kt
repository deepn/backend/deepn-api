package io.deepn.entity

import com.fasterxml.jackson.annotation.JsonProperty
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.time.Instant

@Document("account_activity")
abstract class AccountActivity(
    @Id
    val id: ObjectId = ObjectId.get(),
    val type: AccountActivityType,
    val account: ObjectId,
    val time: Instant = Instant.now()
)

class LoginActivity(val ip: String, account: ObjectId) : AccountActivity(
    type = AccountActivityType.LOGIN,
    account = account
)

@Document("account")
data class BasicAccountInformation(
    @Id
    val id: ObjectId = ObjectId.get(),
    val username: String,
    val email: String,
    @Field("confirmation.code")
    val confirmationCode: String?,
    @Field("login_reset.token")
    val resetToken: String?,
    @Field("login_reset.sendTime")
    val resetTokenTime: Instant?
)

@Document("account")
data class Account(
    @Id
    val id: ObjectId = ObjectId.get(),

    @Indexed(unique = true)
    val username: String,

    @Indexed(unique = true)
    val email: String,

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    val password: String,

    @Field("confirmation.code")
    @JsonProperty("confirmation.code", access = JsonProperty.Access.WRITE_ONLY)
    val confirmationCode: String? = null,

    @Field("confirmation.sendTime")
    @JsonProperty("confirmation.sendTime")
    val confirmationSendTime: Instant? = null,

    @Field("confirmation.confirmed")
    @JsonProperty("confirmation.confirmed")
    val confirmed: Boolean = false,

    @Field("time.creation")
    @JsonProperty("time.creation")
    val creationTime: Instant = Instant.now(),

    @Field("time.update")
    @JsonProperty("time.update")
    val updateTime: Instant = Instant.now(),

    @Field("login_reset.token")
    @JsonProperty("confirmation.code", access = JsonProperty.Access.WRITE_ONLY)
    val resetToken: String? = null,

    @Field("login_reset.sendTime")
    val resetTokenSendTime: Instant? = null,

    val role: AccountRole = AccountRole.ROLE_USER,
)

enum class AccountRole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}

enum class AccountActivityType {
    LOGIN,
}