package io.deepn.entity.strategy.impl

import io.deepn.common.TradingConfiguration
import io.deepn.common.model.BotAddress
import io.deepn.common.model.BotStatus
import io.deepn.common.model.StrategyType
import io.deepn.entity.strategy.Strategy
import org.bson.types.ObjectId
import java.time.Instant

class AlgorithmicStrategy(
    id: ObjectId = ObjectId.get(),
    name: String,
    description: String,
    owner: ObjectId,
    type: StrategyType = StrategyType.ALGORITHMIC,
    exchange: ObjectId,
    configuration: TradingConfiguration,
    status: BotStatus = BotStatus.STOPPED,
    address: BotAddress? = null,
    creationTime: Instant = Instant.now(),
    updateTime: Instant = Instant.now(),
    val source: String
) : Strategy(
    id,
    name,
    description,
    owner,
    type,
    exchange,
    configuration,
    status,
    address,
    creationTime,
    updateTime
)
