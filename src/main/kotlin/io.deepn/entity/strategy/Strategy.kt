package io.deepn.entity.strategy

import com.fasterxml.jackson.annotation.JsonProperty
import io.deepn.common.TradingConfiguration
import io.deepn.common.model.BotAddress
import io.deepn.common.model.BotStatus
import io.deepn.common.model.StrategyType
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.time.Instant

@Document("strategy")
abstract class Strategy(
    @Id
    val id: ObjectId = ObjectId.get(),

    val name: String,
    val description: String,
    val owner: ObjectId,
    val type: StrategyType,
    val exchange: ObjectId,
    val configuration: TradingConfiguration,

    var status: BotStatus = BotStatus.STOPPED,
    var address: BotAddress? = null,

    @Field("time.creation")
    @JsonProperty("time.creation")
    val creationTime: Instant = Instant.now(),

    @Field("time.update")
    @JsonProperty("time.update")
    val updateTime: Instant = Instant.now()
)
