package io.deepn.entity.response

import io.deepn.entity.Account

data class Ok(val success: Boolean = true) {
    constructor(value: Number) : this(value.toLong() > 0)
}

object NoContent

data class TokenResponse(val token: String)

data class LoginResponse(val account: Account, val token: String)

fun Boolean.toResponse() = Ok(this)