package io.deepn.entity.request

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import io.deepn.common.*
import io.deepn.common.model.Exchange
import io.deepn.constant.FieldConstant.CODE_SIZE
import io.deepn.constant.FieldConstant.MAX_SIZE_DESCRIPTION
import io.deepn.constant.FieldConstant.MAX_SIZE_NAME
import io.deepn.constant.FieldConstant.MAX_SIZE_PASSWORD
import io.deepn.constant.FieldConstant.MAX_SIZE_SOURCE
import io.deepn.constant.FieldConstant.MIN_SIZE_NAME
import io.deepn.constant.FieldConstant.MIN_SIZE_PASSWORD
import io.deepn.entity.Account
import io.deepn.entity.ExchangeAccount
import io.deepn.entity.strategy.impl.AlgorithmicStrategy
import io.deepn.validation.*
import org.bson.types.ObjectId
import org.springframework.security.crypto.password.PasswordEncoder
import javax.validation.Valid
import javax.validation.constraints.*

data class LoginRequest(
    @field:NotBlank @field:Email val login: String,
    @field:NotBlank val password: String,
    val stayConnected: Boolean = false
)

data class AccountCreationRequest(
    @field:NotBlank @field:Email @JsonDeserialize(using = ToLowerCase::class) val email: String,
    @field:Size(min = MIN_SIZE_PASSWORD, max = MAX_SIZE_PASSWORD) var password: String,
    @field:Size(min = MIN_SIZE_NAME, max = MAX_SIZE_NAME) @field:Username @field:NoSpace val username: String,
) {
    fun encode(passwordEncoder: PasswordEncoder): AccountCreationRequest {
        this.password = passwordEncoder.encode(password)
        return this
    }

    fun toAccount() = Account(email = email, password = password, username = username)
}

data class ConfirmationCode(@field:NotBlank @field:Size(min = CODE_SIZE, max = CODE_SIZE) val code: String)

data class ResetPasswordRequest(@field:Email @JsonDeserialize(using = ToLowerCase::class) val email: String)

data class PasswordUpdateRequest(
    @field:NotBlank @field:Size(min = MIN_SIZE_PASSWORD, max = MAX_SIZE_PASSWORD) val currentPassword: String,
    @field:NotBlank @field:Size(min = MIN_SIZE_PASSWORD, max = MAX_SIZE_PASSWORD) val newPassword: String
)

data class ResetPasswordUpdateRequest(
    @field:Email @JsonDeserialize(using = ToLowerCase::class) val email: String,
    @field:NotBlank @field:Size(min = MIN_SIZE_PASSWORD, max = MAX_SIZE_PASSWORD) val newPassword: String,
    @field:NotBlank val token: String
)

data class ExchangeAccountRequest(
    @field:Size(
        min = MIN_SIZE_NAME,
        max = MAX_SIZE_NAME
    ) @JsonDeserialize(using = NormalizeSpace::class) val name: String,
    val exchange: Exchange,
    @field:Valid val credentials: ExchangeCredentialsRequest
) {
    fun toExchangeAccount(owner: ObjectId) = ExchangeAccount(
        owner = owner,
        name = name,
        exchange = exchange,
        credentials = credentials.toExchangeCredentials()
    )
}

data class ExchangeCredentialsRequest(
    @field:NotBlank val key: String,
    @field:NotBlank val secret: String,
    @field:NullOrNotBlank val passphrase: String? = null
) {
    fun toExchangeCredentials() = ExchangeCredentials(key, secret, passphrase)
}

data class AlgorithmicStrategyRequest(
    @field:Size(
        min = MIN_SIZE_NAME,
        max = MAX_SIZE_NAME
    ) @JsonDeserialize(using = NormalizeSpace::class) val name: String,
    @field:Size(max = MAX_SIZE_DESCRIPTION) val description: String,
    @field:Size(max = MAX_SIZE_SOURCE) @field:NotBlank val source: String,
    val exchange: ObjectId,
    @field:Valid val configuration: TradingConfigurationRequest,
) {
    fun toStrategy(owner: ObjectId) = AlgorithmicStrategy(
        owner = owner,
        name = name,
        description = description,
        source = source,
        exchange = exchange,
        configuration = configuration.toTradingConfiguration()
    )
}
data class TradingConfigurationRequest(
    @field:NotBlank val quoteCurrency: String,
    @field:NotEmpty val currencies: List<@NotNull String>,
    val timeFrame: TimeFrame,
    val longConfiguration: TradeOperationConfiguration,
    val shortConfiguration: TradeOperationConfiguration,
    val tradeConfiguration: TradeConfiguration
) {
    fun toTradingConfiguration() =
        TradingConfiguration(
            quoteCurrency,
            currencies,
            timeFrame,
            longConfiguration,
            shortConfiguration,
            tradeConfiguration
        )
}
