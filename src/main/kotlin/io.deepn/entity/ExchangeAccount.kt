package io.deepn.entity

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer
import io.deepn.common.ExchangeCredentials
import io.deepn.common.model.Exchange
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.time.Instant

@Document("exchange_account")
data class ExchangeAccount(
    @Id @JsonSerialize(using = ToStringSerializer::class)
    val id: ObjectId = ObjectId.get(),
    val owner: ObjectId,

    val exchange: Exchange,

    val name: String,

    val credentials: ExchangeCredentials,

    @Field("time.creation")
    @JsonProperty("time.creation")
    val creationTime: Instant = Instant.now(),

    @Field("time.update")
    @JsonProperty("time.update")
    val updateTime: Instant = Instant.now()
)
